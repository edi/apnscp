#!/usr/bin/env apnscp_php
<?php declare(strict_types=1);

	use Carbon\Carbon;
	use Carbon\CarbonInterval;
	use Daphnie\Collector;

	define('INCLUDE_PATH', realpath(dirname(__FILE__, 3)));
	include INCLUDE_PATH . '/lib/CLI/cmd.php';
	\Error_Reporter::set_verbose(1);

	if (!TELEMETRY_ENABLED) {
		exit(0);
	}

	define('LAST_RUN_FILE', storage_path('.daphnie.last'));

	function lastRun() {
		if (!file_exists(LAST_RUN_FILE)) {
			return 0;
		}
		$contents = (array)json_decode(file_get_contents(LAST_RUN_FILE), true);
		return array_get($contents, 'now', 0);
	}

	function updateLastRun(int $now) {
		file_put_contents(LAST_RUN_FILE, json_encode(['now' => $now]));
	}

	try {
		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
		\Auth::autoload()->authenticate();
		$handler = new Collector(PostgreSQL::pdo());
		$span = (new Carbon());
		// further collapse historic entries


		$windows = array_combine(
			(array)TELEMETRY_COMPRESSION_THRESHOLD,
			(array)TELEMETRY_COMPRESSION_CHUNK
		);

		$lastRun = lastRun();
		$updatedLastRun = time();

		if (!$windows) {
			warn("Imbalanced compression windows - using defaults");
			$map = \Opcenter\Map::load(config_path('config.ini'))->section('telemetry');
			$k = '[' . $map->offsetGet('compression_threshold') . ']';
			$v = '[' . $map->offsetGet('compression_chunk') . ']';
			$windows = array_combine(
				\Opcenter\CliParser::parseArgs($k),
				\Opcenter\CliParser::parseArgs($v),
			);
		}

		$slide = $updatedLastRun - $lastRun;
		if ($slide <= 0) {
			fatal("Assertion failed: \$updatedLastRun < \$lastRun!");
		}

		debug("Last run TS: %d (%s)", $lastRun, Carbon::createFromTimestamp($lastRun)->format('r'));
		$compressionThreshold = $lastRun;
		$slideInterval = new DateInterval("PT{$slide}S");

		$keys = array_keys($windows);
		$threshold = current($keys);
		$end = Carbon::createFromTimestamp($updatedLastRun)->sub($threshold);
		$begin = clone $end;

		do {
			$chunkSize = $windows[$threshold];
			$oldThreshold = $threshold;
			$threshold = next($keys);
			$begin->subtract($threshold);

			debug(
				"Compression range: [%s, %s) ([%d, %d)) - %s/%s",
				$begin->format('r'),
				$end->format('r'),
				$begin->getTimestamp(),
				$end->getTimestamp(),
				$oldThreshold,
				$chunkSize
			);
			$handler->zip(
				$begin->getTimestamp(),
				$end->getTimestamp(),
				$chunkSize,
				(bool)TELEMETRY_MERGE_DUPLICATES
			);

			$end = clone $begin;
			$begin->sub($threshold)->sub($slideInterval);
		} while (false !== $threshold);
		updateLastRun($updatedLastRun);
	} catch (\apnscpException $e) {
		report("Failed metric zip: %s\n%s", $e->getMessage(), $e->getTraceAsString());
	} finally {
		\Error_Reporter::exception_upgrade($oldex);
	}
