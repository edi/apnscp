#!/usr/bin/env apnscp_php
<?php
	/**
	 * Usage: license.php MODE OPTIONS
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2019
	 */

	namespace app {

		use function cli\get_instance;
		use \Error_Reporter;
		use Opcenter\Apnscp;
		use Opcenter\License;

		include __DIR__ . '/../../lib/CLI/cmd.php';
		Error_Reporter::set_verbose(1);
		$afi = get_instance();
		if (!$afi) {
			fatal("cannot init afi instance");
		}

		function help()
		{
			fwrite(STDERR, "Usage: " . basename(__FILE__) . " MODE" . "\n" .
				"Available modes:" . "\n\n" .
				"issue [-f | --force] CODE [CN]\t: issue a new license using activation CODE. Common name (CN) and force overwrite optional" . "\n" .
				"backup FILENAME\t\t\t: save license at FILENAME" . "\n" .
				"restore FILENAME\t\t: restore x509 license from FILENAME" . "\n" .
				"renew [-f | --force]\t\t: renew license if appropriate" . "\n" .
				"info\t\t\t\t: license information" . "\n"
			);
			exit(1);
		}

		array_shift($argv);
		if (!$argv) {
			help();
		}

		$force = false;
		function forced(&$args) {
			global $force;
			$wantString = is_string($args);
			if (is_string($args)) {
				$args = preg_split('/\s+/', $args, PREG_SPLIT_NO_EMPTY);
			}

			if ($args[0] === '-f' || $args[0] === '--force') {
				$force = true;
				array_shift($args);
			}
			if ($wantString) {
				$args = implode(' ', $args);
			}
		}

		switch ($tmp = array_shift($argv)) {
			case 'issue':
				forced($argv);
				issue(...$argv);
				break;
			case 'backup':
				backup(...$argv);
				break;
			case 'restore':
				restore(...$argv);
				break;
			case 'renew':
				forced($argv);
				renew();
				break;
			case 'info':
				info();
				break;
			default:
				fwrite(STDERR, "Unknown option " . $tmp);
				help();
		}

		/**
		 * Issue license given key
		 *
		 * @param string|null $code
		 * @param string|null $cn
		 */
		function issue(string $code = null, string $cn = null): void
		{
			global $force;

			if (!$code) {
				fatal("Missing activation code");
			}
			$license = License::get();

			if (!$force && !$license->isTrial() && !$license->hasExpired()) {
				fatal("License is neither trial nor expired. Preventing overwrite. Override with -f flag");
			}
			$ret = $license->issue($code, $cn);
			if (!$ret) {
				exit(1);
			}
			reload();
			exit(0);
		}

		/**
		 * Backup license
		 *
		 * @param string|null $path
		 */
		function backup(string $path = null): void
		{
			if (!(string)License::get()) {
				fatal("No license detected in `%s'", License::get()->getLicensePath());
			}
			if (!$path) {
				$path = tempnam('/root/', 'apnscp-license');
				info("License saved to `%s'", $path);
			} else if (file_exists($path)) {
				fatal("Cannot save license: path `%s' exists", $path);
			}
			touch($path) && chmod($path, 0600);
			$ret = file_put_contents($path, (string)License::get()) > 0;
			exit ($ret ^ 1);
		}

		/**
		 * Restore certificate from backup
		 *
		 * @param string|null $path x509 or zip
		 */
		function restore(string $path = null): void
		{
			if (!file_exists($path)) {
				fatal("Key `%s' does not exist", $path);
			}

			if (preg_match('/\.zip$/', $path))
			{
				// file_stat() doesn't work via stream wrapper :(
				$zip = new \ZipArchive();
				if ($zip->open($path) && (false !== $zip->locateName('license.pem'))) {
					$zip->close();
					$zippath = 'zip://' . $path . '#license.pem';
					$path = tempnam(TEMP_DIR, 'license');
					register_shutdown_function(function() use ($path) {
						file_exists($path) && unlink($path);
					});

					if (!copy($zippath, $path)) {
						fatal('Failed to extract license.pem from %s', $path);
					}
				}
			}

			if (!($license = new License($path))->verify()) {
				exit(1);
			}
			if ($ret = $license->install()) {
				reload();
			}
			exit($ret^1);
		}

		/**
		 * Renew certificate
		 */
		function renew(): void
		{
			global $force;

			if (!License::get()->needsReissue() && !$force) {
				$msg = License::get()->hasExpired() ? "License has expired and will not be renewed without --force flag" :
					"License does not need reissuance";
				info($msg);
				exit(1);
			}

			$ret = (License::get()->reissue() && reload()) ^ 0;
			exit($ret);
		}

		function info(): void
		{
			$license = new License(getenv('LICENSE') ?: null);
			if (!(string)$license) {
				fatal("No license detected in `%s'", License::get()->getLicensePath());
			}
			$view = \BladeLite::factory(webapp_path('license/views'));
			echo html_entity_decode(preg_replace('/$[\r\n]+^\s*([^\r\n]+)$/m', "\\1\n", strip_tags((string)$view->render('license-data', ['license' => $license]))));
		}

		function reload(): void
		{
			if (!serial(static function () {
				return Apnscp::running() ? Apnscp::restart() : Apnscp::run('start');
			})) {
				warn("Failed to restart %s - restart manually as systemctl restart apiscp", PANEL_BRAND);
			}

			\info("License installed. %s restarting", PANEL_BRAND);
		}
	}
