---
title: "Installing new Rubies"
date: "2015-05-05"
---

## Overview

[v6+ platforms](https://kb.apiscp.com/platform/determining-platform-version/) support multiple Ruby interpreters through [rvm](https://kb.apiscp.com/ruby/changing-ruby-versions/). If a Ruby interpreter doesn't appear via `rvm list`, simply open a ticket to request a new Ruby version to be installed.
