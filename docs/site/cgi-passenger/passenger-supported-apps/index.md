---
title: "Passenger-supported apps"
date: "2015-02-28"
---

## Overview

Passenger supports Python, Ruby, Node.js, Meteor as well as any application built using this technology. In creating several of the articles in this knowledgebase, we also created a very basic application to guide during writing. These applications are up and running under a sandbox domain and represent just a sliver of the applications that you can run using Passenger.

- [Ghost demo](http://ghost.sandbox.apiscp.com/) ([how-to](https://kb.apiscp.com/guides/installing-ghost/))
- [Meteor demo](http://meteor.sandbox.apiscp.com/) ([how-to](https://kb.apiscp.com/guides/running-meteor/))
- [Simple Node.js demo](http://node.sandbox.apiscp.com/) ([how-to](https://kb.apiscp.com/guides/running-node-js/))
- [Express demo](http://express.sandbox.apiscp.com/) ([how-to](https://kb.apiscp.com/guides/installing-express/))
- [Rails 4 with Ruby 2.2 demo](http://rails4.sandbox.apiscp.com/) ([how-to](https://kb.apiscp.com/ruby/setting-rails-passenger/))
- [Rails 3 with Ruby 1.9 demo](http://rails3.sandbox.apiscp.com/)
- [Rails 2 with Ruby 1.8 demo](http://rails2.sandbox.apiscp.com/)
- [Python WSGI with Python 3 demo](http://py3.sandbox.apiscp.com/) ([how-to](https://kb.apiscp.com/python/using-wsgi/))
- [Python WSGI with Python 2 demo](http://py2.sandbox.apiscp.com/)
- [Django demo](http://django.sandbox.apiscp.com/) ([how-to](https://kb.apiscp.com/python/django-quickstart/))
- [Flask demo](http://flask.sandbox.apiscp.com) ([how-to](https://kb.apiscp.com/python/flask-quickstart/))
