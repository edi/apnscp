<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\CliParser;
	use Opcenter\Http\Apache;
	use Tivie\HtaccessParser\Parser;
	use Tivie\HtaccessParser\Token\Directive;

	class Evasive implements SettingsInterface
	{
		const CONFIG = '/etc/httpd/conf.d/evasive.conf';
		const MAP = [
			'hashtablesize' => 'hash-table-size',
			'pagecount' => 'page-count',
			'sitecount' => 'site-count',
			'pageinterval' => 'page-interval',
			'siteinterval' => 'site-interval',
			'blockingperiod' => 'blocking-period',
			'httpstatus' => 'http-status',
			'logdir' => 'log-dir',
			'enabled' => 'enabled',
			'canonicalize' => 'canonicalize'
		];

		// allowable hash size
		public const PRIME_LIST = [
			53,			97,			193,		389,		769,
			1543,		3079,		6151,		12289,		24593,
			49157,		98317,		196613,		393241,		786433,
			1572869,	3145739,	6291469,	12582917,	25165843,
			50331653,	100663319,	201326611,	402653189,	805306457,
			1610612741,	3221225473,	4294967291
		];

		protected $parser;

		public function set($var, ...$val): bool
		{
			if (\is_array($var)) {
				foreach ($var as $k => $v) {
					if (!$this->set($k, $v)) {
						return false;
					}
				}

				return true;
			}
			if (!isset($val[0])) {
				return error("Missing value for `%s'", $var);
			}
			if (!\in_array($var, static::MAP, true)) {
				return error("Unknown mod_evasive directive `%s'", $var);
			}

			$val = $val[0];
			$activeSettings = $this->get();

			if (isset($activeSettings[$var]) && $activeSettings[$var] == $val) {
				return true;
			}

			if ($var === 'hash-table-size') {
				// lookup hash size
				foreach (static::PRIME_LIST as $prime) {
					if ($prime < $val) {
						continue;
					}
					break;
				}
				if ($prime !== $val) {
					info('Rounded hash to nearest supported table size, %d', $prime);
					$val = $prime;
				}
			} else if ($var === 'enabled' || $var === 'canonicalize') {
				$val = ($val === true || !strcasecmp((string)$val, 'on')) ? 'on' : 'off';
			}

			$parser = $this->getParser()->parse();
			if (!($block = $parser->search('IfModule'))) {
				return error("Missing <IFModule directive in `%s'. This file is corrupt", self::CONFIG);
			}
			$directiveName = 'DOS' . studly_case($var);
			$found = false;

			for ($i = \count($block[0])-1; $i >= 0; $i--) {
				// only last directive matters
				$blockDirective = $block[0][$i];
				$blockDirectiveName = strtolower(substr($blockDirective->getName(), 3));

				if (!isset(static::MAP[$blockDirectiveName])) {
					continue;
				}
				$inputName = static::MAP[$blockDirectiveName];
				if ($inputName === $var) {
					if ($val == $blockDirective->getValue()) {
						return true;
					}
					$found = true;
					// setArguments = append = W.T.F.
					foreach ($blockDirective->getArguments() as $arg) {
						$blockDirective->removeArgument($arg);
					}
					$blockDirective->addArgument($val, true);
					break;
				}
			}
			if (!$found) {
				$block[0]->addChild(new Directive($directiveName, [$val]));
			}
			file_put_contents(self::CONFIG, (string)$parser, LOCK_EX);
			Apache::reload();

			return true;
		}

		public function get(...$var)
		{
			if (!file_exists(self::CONFIG)) {
				return [];
			}
			$parser = $this->getParser();
			$vars = [];
			$block = $parser->parse()->search('IfModule');
			/** @var Directive $directive */
			foreach ($block[0] as $directive) {
				$directiveName = $directive->getName();
				if (strncasecmp($directiveName, 'dos', 3) !== 0) {
					continue;
				}
				$cfgName = strtolower(substr($directiveName, 3));
				if ($cfgName === 'whitelist') {
					// handled by apacher.evasive-whitelist
					continue;
				}
				if (!isset(static::MAP[$cfgName])) {
					warn("Unrecognized mod_evasive setting - skipping `%s'", $directive->getName());
				}
				$vars[static::MAP[$cfgName]] = \Util_Conf::inferType($directive->getValue());
			}

			$vars['enabled'] = empty($vars['enabled']) || !strcasecmp($vars['enabled'], 'on');

			return $vars;
		}

		private function getParser(): Parser
		{
			if (null === $this->parser) {
				$this->parser = new Parser(new \SplFileObject(self::CONFIG));
				$this->parser->ignoreComments(true);
				$this->parser->ignoreWhitelines(true);
			}
			$this->parser->setContainer(new Apache\UnboundedHtaccessContainer());

			return $this->parser;
		}

		public function getHelp(): string
		{
			return 'General mod_evasive configuration';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return 'mixed';
		}
	}