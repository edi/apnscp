<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Cron;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Map;

	class Notify implements SettingsInterface
	{
		public const ANACRON_CONF = '/etc/anacrontab';

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}
			if (false !== strpos($val, '@') && !preg_match(\Regex::EMAIL, $val)) {
				return error('invalid email address');
			}
			$cfg = new Config();
			$cfg['anacron_address'] = $val;
			unset($cfg);
			Bootstrapper::run('apnscp/crons');

			return true;
		}

		public function get()
		{
			if (!file_exists(self::ANACRON_CONF)) {
				return null;
			}

			$map = Map::load(self::ANACRON_CONF, 'r', 'inifile');

			return $map['MAILTO'] ?? (new Config())['anacron_address'] ?? $this->getDefault();
		}

		public function getDefault()
		{
			return 'root';
		}

		public function getHelp(): string
		{
			return 'Set notification email for nightly cron tasks';
		}

		public function getValues()
		{
			return 'string';
		}
	}