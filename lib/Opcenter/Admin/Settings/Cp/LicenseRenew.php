<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\License;

	class LicenseRenew implements SettingsInterface
	{
		public function get()
		{
			return (new License())->daysUntilExpire();
		}

		public function set($val): bool
		{
			if ((int)$val !== 1) {
				return true;
			}
			$license = new License();
			if ($license->isLifetime()) {
				return warn('License is lifetime - reissuance unnecessary');
			}

			return (new License())->reissue(true);

		}

		public function getHelp(): string
		{
			return 'Get license days-until-expire or force renewal of license';
		}

		public function getValues()
		{
			return 'int';
		}

		public function getDefault()
		{
			return null;
		}
	}