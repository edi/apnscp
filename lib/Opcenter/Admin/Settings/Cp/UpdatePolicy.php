<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Map;

	class UpdatePolicy implements SettingsInterface
	{
		const EDGE_LOCK = '.edge.lock';

		const UPDATE_POLICIES = [
			'edge',
			'edge-major',
			'minor',
			'major',
			'all',
			false
		];

		public function set($val): bool
		{
			if ($val === 'default') {
				// consistency with system.update-policy
				$val = $this->getDefault();
			}
			if ($val === $this->get()) {
				return true;
			}

			if (!\in_array($val, static::UPDATE_POLICIES, true)) {
				return error("Unknown update policy `%s'", $val);
			}
			$lockFile = storage_path(self::EDGE_LOCK);
			if ($val === 'edge-major') {
				file_put_contents($lockFile, strtok(\Opcenter::getReleaseTag(), '-'));
				$val = 'edge';
			} else if (file_exists($lockFile)) {
				unlink($lockFile);
			}
			$cfg = new Config();
			$cfg['apnscp_update_policy'] = $val;
			unset($cfg);
			$sysconf = Map::load('/etc/sysconfig/apnscp', 'w', 'inifile');
			$sysconf->section(null)->set('APNSCP_UPDATE_POLICY', (string)$val);
			$sysconf->close();
			Bootstrapper::run('apnscp/crons');

			return true;
		}

		public function get()
		{
			$config = new Config();

			$policy = $config['apnscp_update_policy'];
			if ($policy === 'edge' && file_exists($file = storage_path(self::EDGE_LOCK))) {
				return 'edge-major';
			}

			return $policy;
		}

		public function getHelp(): string
		{
			return 'apnscp update policy';
		}

		public function getValues()
		{
			return self::UPDATE_POLICIES;
		}

		public function getDefault()
		{
			return 'major';
		}
	}