<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Admin\Settings\Argos;

	use Opcenter\Admin\Settings\SettingsInterface;

	class Config implements SettingsInterface
	{

		public function get(...$key)
		{
			if (!isset($key[0])) {
				warn('Configuration must specify backend - call argos.backends to list backends');
				return null;
			}

			return \apnscpFunctionInterceptor::init()->call('argos_get_config', $key);
		}

		public function set($val, ...$xtra): bool
		{
			if (empty($xtra)) {
				return error("Configuration must specify backend or '' to apply configuration to all backends");
			}
			if (!$val) {
				$val = \Opcenter\Argos\Config::get()->getBackends();
			}
			if (isset($xtra[1])) {
				$xtra = [$xtra[0] => $xtra[1]];
			} else {
				$xtra = $xtra[0];
			}
			foreach ((array)$val as $backendName) {
				if (!\apnscpFunctionInterceptor::init()->call('argos_config_relay', [$backendName, $xtra])) {
					return false;
				}

			}

			return true;
		}

		public function getHelp(): string
		{
			return 'Set an Argos directive. Provide backend or "" to apply to all backends';
		}

		public function getValues()
		{
			return 'mixed';
		}

		public function getDefault()
		{
			return 'mixed';
		}
	}