<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Mail\Services;

	use Opcenter\Mail\Contracts\FilterProvider;
	use Opcenter\System\GenericSystemdService;

	class Spamassassin extends GenericSystemdService implements FilterProvider
	{
		public static function reload(string $timespec = null): bool
		{
			if (MAIL_SPAM_FILTER !== 'spamassassin') {
				return true;
			}
			return parent::reload($timespec);
		}

		public static function restart(string $timespec = null): bool
		{
			if (MAIL_SPAM_FILTER !== 'spamassassin') {
				return true;
			}
			return parent::restart($timespec);
		}

		public static function present(): bool
		{
			return MAIL_SPAM_FILTER === 'spamassassin';
		}
	}