<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2018
	 */

	namespace Opcenter\Service\Validators\Mlist;

	use Opcenter\Mail\Services\Majordomo;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceToggle;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall, ServiceToggle
	{
		use \FilesystemPathTrait;

		public const DESCRIPTION = 'Mailing list support';
		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function suspend(SiteConfiguration $svc): bool
		{
			$dir = (new Majordomo($svc->getAccountRoot()))->getListHome();
			if (is_dir($dir)) {
				chmod($dir, 000);
			}

			return true;
		}

		public function activate(SiteConfiguration $svc): bool
		{
			$dir = (new Majordomo($svc->getAccountRoot()))->getListHome();
			if (is_dir($dir)) {
				chmod($dir, 0711);
			}

			return true;
		}


	}

