<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */


	namespace Opcenter\Service\Validators\Rampart;

	use Opcenter\Net\Firewall\Delegated;
	use Opcenter\Net\Firewall\Ipset;
	use Opcenter\Net\Ip4;
	use Opcenter\Net\Ip6;
	use Opcenter\Service\Contracts\ServiceExplicitReconfiguration;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Validators\Common\GenericMap;
	use Opcenter\SiteConfiguration;

	class Whitelist extends GenericMap implements ServiceInstall, ServiceExplicitReconfiguration
	{
		const MAP_FILE = 'whitelist';
		const DESCRIPTION = 'IPv4 + IPv6 addresses';
		const VALUE_RANGE = 'IPv4 | IPv6';

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue(null, 'enabled')) {
				$value = [];
				return true;
			}

			if (!\is_array($value)) {
				$value = (array)$value;
			}

			$bad = array_filter($value, static function ($val) {
				return false !== strpos($val, '/') || ( !Ip4::valid($val) && !Ip6::valid($val) );
			});
			if ($bad) {
				return error('invalid IPs found: %s', implode(',', $bad));
			}

			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			// $old is null if the service is first introduced
			$old = (array)$old;
			$new = (array)$new;
			// merge key into value if hextet
			foreach ($old as $k => $v) {
				if (false === strpos($v, ':')) {
					continue;
				}
				$old[$k] = "$k:$v";
			}
			$old = array_values($old);
			$add = array_diff($new, $old);
			$remove = array_diff($old, $new);
			foreach ($add as $ip) {
				$bound = Delegated::getDelegators($ip);
				if (\in_array($this->site, $bound, true)) {
					continue;
				}
				$setName = Delegated::IPSET_NAME . (Ip6::valid($ip) ? '6' : '');
				if (!Ipset::add($setName, $ip)) {
					warn("Failed to add IP address `%s'", $ip);
				} else {
					info("Whitelisted `%s'", $ip);
				}
				Delegated::markDelegated($ip, $this->site);
			}
			foreach ($remove as $ip) {
				$bound = Delegated::getDelegators($ip);
				$bound = array_diff($bound, [$this->site]);
				Delegated::releaseDelegation($ip, $this->site);
				if (!empty($bound)) {
					info("`%s' still whitelisted by: %s", $ip, implode(', ', $bound));
					// other sites refer to IP
					continue;
				}
				// no further references, discard
				info("Removed whitelist `%s'", $ip);
				$setName = Delegated::IPSET_NAME . (Ip6::valid($ip) ? '6' : '');
				if (!Ipset::remove($setName, $ip)) {
					warn("Failed to remove IP address `%s' from ipset `%s'", $ip, $setName);
				}
			}

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}


		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure([], $this->ctx->getServiceValue(null, 'whitelist', []), $svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure($this->ctx->getServiceValue(null, 'whitelist', []), [], $svc);
		}

		public function write() {
			$addrs = $this->ctx->getServiceValue(null, 'whitelist');
			if (!$addrs) {
				return '[]';
			}
			return '[\'' . implode('\',\'', $addrs) . '\']';
		}

	}
