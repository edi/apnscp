<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ipinfo;

	use Opcenter\Mail\Services\Postfix;
	use Opcenter\Map;
	use Opcenter\Net\Iface;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Contracts\ServiceToggle;
	use Opcenter\Service\Validators\Common\IpValidation;
	use Opcenter\SiteConfiguration;

	class Ipaddrs extends IpValidation implements AlwaysValidate, ServiceReconfiguration, ServiceToggle
	{
		const VALUE_RANGE = '[addr1, addr2...]';
		const DESCRIPTION = 'Assign IP addresses uniquely to account';

		public function valid(&$value): bool
		{
			$this->autoToggle($value);

			if (!$this->ctx['enabled'] || $this->ctx['namebased']) {
				$value = [];

				return true;
			}
			if (empty($value)) {
				$allocationClass = '\\Opcenter\\Net\\' . $this->getIpClass();
				$ip = $allocationClass::allocate();
				if (!$ip) {
					return error('missing IP address for non-namebased');
				}
				info("assigning `%s' to site", $ip);
				$value = [$ip];
			}

			if (!$this->ctx->hasOld() || $this->ctx->serviceValueChanged(null, 'ipaddrs')) {
				$map = Map::load($this->getMap(), 'cl');
				foreach ($value as $ip) {
					if ($site = $map->fetch($ip)) {
						return error("IP `%s' already in use by `%s'", $ip, $site);
					}
				}
			}

			return true;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure([], (array)$this->ctx['ipaddrs'], $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$map = Map::load($this->getMap(), 'cd');
			$assignmentClass = '\\Opcenter\\Net\\' . $this->getIpClass();
			foreach (array_diff($new, $old) as $ip) {
				// we don't track interfaces yet
				$assignmentClass::assign($ip, Iface::ip_iface($ip) ?? Iface::detect());
				Iface::bound($ip) || error("failed to bind ip address `%s'", $ip);
				$map->set($ip, $svc->getSite());
			}
			foreach (array_diff($old, $new) as $ip) {
				$assignmentClass::release($ip);
				Iface::bound($ip) && error("failed to unbind ip address `%s'", $ip);
				$map->fetch($ip) === $svc->getSite() && $map->delete($ip, $svc->getSite());
				if (!$this->ctx->isDelete()) {
					$this->checkLostBindings($svc->getSite(), $ip);
				}
			}
			$map->save();

			return true;
		}

		/**
		 * Scan mail configuration for potential losses
		 *
		 * @param string $site
		 * @param string $ip
		 */
		protected function checkLostBindings(string $site, string $ip)
		{

			foreach (Postfix::siteOverrides($site) as $override) {
				if (substr($override, -3) !== '.cf') {
					// previously disabled or junk
					continue;
				}

				$contents = file_get_contents($override);
				if (!preg_match('!\b' . preg_quote($ip, '!') . '\b!', $contents)) {
					continue;
				}
				warn("IP `%(ip)s' referenced in `%(file)s', but no longer attached to site. Disabling configuration.",
					['ip' => $ip, 'file' => $override]
				);
				rename($override, "$override.disabled");
			}
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure($this->ctx['ipaddrs'], [], $svc);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		protected function getPool(): array
		{
			$class = $this->getIpClass();
			return $class::nb_pool();
		}

		protected function getMap(): string
		{
			return $this->getIpClass() === 'Ip4' ? 'ipmap' : 'ipmap6';
		}

		protected function getIpClass(): string
		{
			return static::class === self::class ? 'Ip4' : 'Ip6';
		}

		public function suspend(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function activate(SiteConfiguration $svc): bool
		{
			// confirm IPs are up to avoid Apache from blowing up
			$assignmentClass = '\\Opcenter\\Net\\' . $this->getIpClass();
			foreach ($this->ctx['ipaddrs'] as $addr) {
				if (!Iface::bound($addr)) {
					$assignmentClass::assign($addr, Iface::ip_iface($addr) ?? Iface::detect());
				}
			}
			return true;
		}


	}