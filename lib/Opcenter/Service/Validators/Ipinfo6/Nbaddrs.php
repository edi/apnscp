<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ipinfo6;

	use Opcenter\Net\Ip6;
	use Opcenter\Service\Validators\Common\IpValidation;
	use Opcenter\SiteConfiguration;

	class Nbaddrs extends \Opcenter\Service\Validators\Ipinfo\Nbaddrs
	{
		const VALUE_RANGE = '[null, [addr1, addr2...]]';
		const DESCRIPTION = 'Assign shared addresses to account. Leave blank to select round-robin';

		public function valid(&$value): bool
		{
			if ($value && !isset($value[0])) {
				$value = array_key_map(static function ($k, $v) {
					return "$k:$v";
				}, (array)$value);
			}
			return parent::valid($value); // TODO: Change the autogenerated stub
		}


		public function populate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return true;
		}


	}
