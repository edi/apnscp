<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Reseller;

	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\Service\Validators\Common\GenericMap;
	use Opcenter\SiteConfiguration;

	class ResellerId extends GenericMap implements ServiceReconfiguration
	{
		const MAP_FILE = 'reseller.map';
		const DESCRIPTION = 'Reseller ID declaration';

		public function valid(&$value): bool
		{
			if (!RESELLER_ENABLED && $value !== 0) {
				warn('Reseller support is disabled. Defaulting to 0');
				$value = 0;
			}

			$oldId = $this->ctx->getOldServiceValue(null, 'reseller_id');
			if (static::class === self::class && $oldId && $oldId !== $value) {
				return error("Reseller ID may not change");
			}

			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			return $old === $new;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}


	}