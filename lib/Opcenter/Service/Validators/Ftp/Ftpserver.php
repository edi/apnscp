<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Ftp;

	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Validators\Common\GenericDomainMap;

	class Ftpserver extends GenericDomainMap implements AlwaysValidate
	{
		const MAP_FILE = 'ftp.domainmap';
		const DESCRIPTION = 'FTP server prefix';
	}
