<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace Opcenter;

class Reseller {
	const RESELLER_PREFIX = 'group';

	/**
	 * Parse ID from path
	 *
	 * @param string $path
	 * @return Identity|null
	 */
	public static function idFromPath(string $path): ?int
	{
		$resellerBase = FILESYSTEM_VIRTBASE . '/' . self::RESELLER_PREFIX;
		if (0 !== strpos($path, $resellerBase)) {
			return null;
		}
		$id = substr($path, $len = strlen($resellerBase), strpos($path . '/', '/', $len) - $len);
		if (ctype_digit($id)) {
			return (int)$id;
		}

		return null;
	}

	/**
	 * Reseller support enabled
	 *
	 * @return bool
	 */
	public static function enabled(): bool
	{
		return (bool)RESELLER_ENABLED;
	}

	public static function pathFromid(int $id): string
	{
		return FILESYSTEM_VIRTBASE . '/' . self::RESELLER_PREFIX . $id;
	}
}


