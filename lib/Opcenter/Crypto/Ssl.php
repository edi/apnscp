<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, April 2019
 */


namespace Opcenter\Crypto;

use DateTime;
use Opcenter\Versioning;
use Regex;

class Ssl
{
	/** @param string location for server.pem certificate */
	public const SYSTEM_CERT_PATH = '/etc/pki/tls/certs';

	/**
	 * Get system storage certificate path
	 *
	 * @return string
	 */
	public static function systemCertificatePath(): string
	{
		return self::SYSTEM_CERT_PATH . DIRECTORY_SEPARATOR . 'server.pem';
	}

	/**
	 * Get hostnames for which a certificate is valid
	 *
	 * @param resource|string $certificate
	 * @return array
	 */
	public static function alternativeNames($certificate): ?array
	{
		$certificate = static::parse($certificate);
		if (!\is_array($certificate)) {
			error('invalid certificate');

			return null;
		}
		$commonname = $certificate['subject']['CN'];
		$extensions = array($commonname);
		if (isset($certificate['extensions']['subjectAltName'])) {
			$alt = $certificate['extensions']['subjectAltName'];
			foreach (explode(',', $alt) as $name) {
				$name = trim($name);
				if (strncmp($name, 'DNS:', 4)) {
					report('gibberish line? %s certificate: %s',
						$name,
						var_export($certificate, true)
					);
					continue;
				}
				$tmp = substr($name, 4);
				if ($tmp !== $commonname) {
					$extensions[] = $tmp;
				}
			}
		}

		return $extensions;
	}

	/**
	 * Certificate issuer matches subject
	 *
	 * @param array|string $crt
	 * @return bool
	 */
	public static function selfSigned($crt): bool
	{
		$crt = static::parse($crt);
		// self-signed will be suitable for
		// alternative calculation, let's evaluate this first

		if (!isset($crt['issuer'], $crt['subject'])) {
			return error('invalid certificate');
		}
		$a = $crt['issuer'];
		$b = $crt['subject'];

		foreach ($a as $k => $v) {
			if (!isset($b[$k]) ||
				$b[$k] != $v
			) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Parse certificate into components
	 *
	 * @param string|array $crt
	 * @return array|bool|resource|void
	 */
	public static function parse($crt) {
		if (!$crt) {
			return error('no certificate!');
		}

		if (\is_array($crt)) {
			if (isset($crt['purposes'])) {
				// assume it's already parsed
				return $crt;
			} else {
				return error('unsupported certificate data type, ' .
					'got array expected: string, resource, or parsed certificate');
			}
		}
		if (!\is_resource($crt)) {
			if (self::isDer($crt)) {
				$crt = self::der2Pem($crt);
			}
			$crt = openssl_x509_read($crt);
			if (!$crt) {
				return error('unable to parse certificate: %s', self::error());
			}
		}
		$info = openssl_x509_parse($crt);
		if (\is_resource($crt)) {
			openssl_x509_free($crt);
		}
		if (!$info) {
			return error(static::error());
		}

		return $info;
	}

	/**
	 * Extract certificate from PEM
	 *
	 * @param string $pem
	 * @return string
	 */
	public static function extractCertificate(string $pem): ?string
	{
		$output = null;
		if (!openssl_x509_export($pem, $output)) {
			error(self::error());

			return null;
		}

		return $output;
	}

	/**
	 * Split PEM-formatted X509 across boundaries
	 * @param string $crt
	 * @return array
	 */
	public static function splitChain(string $crt): array
	{
		if (!preg_match_all('/-----BEGIN CERTIFICATE-----[^- ]+-----END CERTIFICATE-----/mS', $crt, $matches, PREG_SET_ORDER)) {
			warn('No certificates found in supplied PEM');
			return [];
		}
		return array_column($matches, 0);
	}

	/**
	 * Certificate is DER format
	 *
	 * @param string $cert
	 * @return bool
	 */
	public static function isDer(string $cert): bool
	{
		return 0 !== strncmp($cert, '-----', 5);
	}

	public static function der2Pem(string $data, string $decorator = 'CERTIFICATE'): string
	{
		$pem = chunk_split(base64_encode($data), 64, "\n");
		$pem = "-----BEGIN $decorator-----\n" . $pem . "-----END $decorator-----\n";

		return $pem;
	}

	public static function pem2Der($data): string
	{
		$data = trim($data);
		$begin = '-----';
		$end = '-----END';
		$data = substr($data, strpos($data, $begin, 1) + \strlen($begin));
		$data = substr($data, 0, strpos($data, $end));
		return base64_decode($data);
	}

	/**
	 * Copy last OpenSSL error triggered by
	 * openssl_* function to error buffer
	 * {@link Error_Reporter::add_error}
	 *
	 * @return bool
	 */
	private static function error(): ?string
	{
		$err = openssl_error_string();
		if (!$err) {
			return null;
		}
		[$class, $code, $loc, $sym, $txt] = explode(':', $err);

		return sprintf("%s: openssl `%s': %s (%s)",
			$code, $class, $loc, $txt);
	}

	/**
	 * Generate SSL private key
	 *
	 * @param int $bits
	 * @return bool|void
	 */
	public static function genkey(int $bits = 2048) {
		if ($bits < 2048 && version_compare(Versioning::asMinor(os_version()), '8.0', '>=')) {
			// vsftpd pukes with 1024 bit keys, assume it's an OpenSSL setting
			return error('Bit length too weak for this version of OpenSSL');
		}
		$bits = \intval($bits);
		$pow = log($bits) / log(2);
		if ($pow - ceil($pow) > 0) {
			return error('pkey bits %d invalid', $bits);
		}
		if ($bits < 384) {
			return error('pkey must be at least 384 bits');
		}
		if ($bits > 8192) {
			return error('8192 bit pkey limit');
		}

		$digestalg = 'sha1';
		if (\function_exists('openssl_get_md_methods')) {
			$methods = openssl_get_md_methods();
			if (\in_array('sha512', $methods, true)) {
				$digestalg = 'sha512';
			} else if (\in_array('sha256', $methods, true)) {
				$digestalg = 'sha256';
			} else if (!\in_array('sha1', $methods, true)) {
				return error('no suitable digest method found for privkey generation');
			}
		}

		$opts = array(
			'private_key_bits' => $bits,
			'private_key_type' => OPENSSL_KEYTYPE_RSA,
			'digest_alg'       => $digestalg
		);
		$res = openssl_pkey_new($opts);
		if (!$res) {
			return error('private key generation failed! error: %s',
				self::error());
		}
		openssl_pkey_export($res, $key);

		return $key;
	}

	/**
	 * Sign request
	 *
	 * @param string   $csr
	 * @param string   $privkey
	 * @param int      $days
	 * @param int|null $serial
	 * @return bool|void
	 * @throws \Exception
	 */
	public static function selfsign(
		string $csr,
		string $privkey,
		int $days = 365,
		int $serial = null
	) {
		if ($days > 365 * 5) {
			return error('max certificate validity 5 years');
		}
		if ($days < 1) {
			return error('invalid certificate validity');
		}

		$csr = trim($csr);
		if (!$serial) {
			$serial = (int)sprintf('%s', date_format(new DateTime(), 'YmdHis'));
		} else if ($serial < 0) {
			return error('Serial must be a positive integer');
		}

		if (!openssl_csr_get_public_key($csr)) {
			return error('invalid CSR');
		}

		$crt = openssl_csr_sign($csr, null, $privkey, $days, array(), $serial);

		if (!$crt) {
			return error(self::error());
		}
		if (!openssl_x509_export($crt, $certout)) {
			return error(self::error());
		}

		return $certout;
	}

	/**
	 * Generate certificate signing request
	 *
	 * @param string $privkey
	 * @param string $host
	 * @param string $country
	 * @param string $state
	 * @param string $locality
	 * @param string $org
	 * @param string $orgunit
	 * @param string $email
	 * @param array  $san
	 * @return bool|string
	 */
	public static function generate_csr(
		string $privkey,
		string $host,
		string $country = '',
		string $state = '',
		string $locality = '',
		string $org = '',
		string $orgunit = '',
		string $email = '',
		array $san = []
	) {
		$sinfo = array(
			'countryName'            => strtoupper($country),
			'stateOrProvinceName'    => $state,
			'localityName'           => $locality,
			'organizationName'       => $org,
			'organizationalUnitName' => $orgunit,
			'commonName'             => $host,
			'emailAddress'           => $email
		);
		if (!preg_match(Regex::DOMAIN_WC, $host)) {
			return error("invalid hostname `%s'", $host);
		} else if ($sinfo['countryName'] && (!ctype_alpha($sinfo['countryName']) ||
				\strlen($sinfo['countryName']) !== 2)) {
			return error("invalid 2-character country `%s'",
				$sinfo['countryName']);
		} else if (!$sinfo['stateOrProvinceName']) {
			return error('no state value specified');
		} else if (!$sinfo['localityName']) {
			return error('missing state/locality name');
		} else if ($sinfo['emailAddress'] !== '' &&
			!preg_match(Regex::EMAIL, $sinfo['emailAddress'])) {
			return error("invalid e-mail address `%s'",
				$sinfo['emailAddress']);
		}
		foreach ($sinfo as $k => $v) {
			if (!$v) {
				unset($sinfo[$k]);
			}
		}
		$privkey = trim($privkey);
		if (!$privkey = openssl_pkey_get_private($privkey)) {
			return error('could not get key structure from private key');
		}

		$extensions = [];
		if ($san) {
			foreach ($san as $s) {
				if (0 !== strncmp($s, '*.', 2) && !preg_match(Regex::DOMAIN, $s)) {
					return error("Invalid domain `%s'", $s);
				}
			}
			$extensions['subjectAltName'] = implode(', ', array_key_map(static function ($k, $v) {
				return 'DNS.' . ($k + 1) . ': ' . $v;
			}, array_values($san)));
		}

		$cnf = array('digest_alg' => 'sha256', 'req_extensions' => $extensions);

		$res = openssl_pkey_get_private($privkey);
		$csr = openssl_csr_new($sinfo, $res, $cnf);

		if (!$csr) {
			return error(self::error());
		}

		$txt = null;
		if (!openssl_csr_export($csr, $txt)) {
			return error(self::error());
		}

		return $txt;
	}

	/**
	 * Get certificate request information
	 *
	 * @param string $csr
	 * @return array|bool
	 */
	public static function request_info(string $csr)
	{
		$res = openssl_csr_get_subject($csr);
		if (!$res) {
			return error(self::error());
		}

		return $res;
	}

	/**
	 * Collapse
	 *
	 * @param array       $params key, crt, chain
	 * @param string|null $base optional base directory to prepend
	 * @return string|null
	 */
	public static function unify(array $params, string $base = null): ?string
	{
		$files = array_map(static function ($p) use ($base) {
			return self::absolutePath($p, $base);
		}, array_intersect_key($params, array_fill_keys(['key', 'chain', 'crt'], 1)));
		return ltrim(implode("\n", array_map(static function ($f) {
			if (!file_exists($f)) {
				fatal("Requested SSL resource `%s' missing", $f);
			}
			if (!$data = trim(file_get_contents($f))) {
				fatal("Invalid/erroneous SSL resource `%s'", $f);
			}
			return $data;
		}, $files))) ?: null;
	}

	/**
	 * Convert resource to absolute path given type
	 *
	 * @param string      $resource
	 * @param string|null $base
	 * @return string
	 */
	public static function absolutePath(string $resource, string $base = null): string
	{
		if ($resource[0] === '/') {
			return $resource;
		}

		if (!$base) {
			fatal("Relative path provided - expected absolute path `%s'", $resource);
		}

		$ext = substr($resource, strrpos($resource, '.'));
		$base = rtrim($base, '/');
		switch ($ext) {
			case '.crt':
				return $base . \Ssl_Module::CRT_PATH . "/${resource}";
			case '.key':
				return $base . \Ssl_Module::KEY_PATH . "/${resource}";
			case '.csr':
				return $base . \Ssl_Module::CSR_PATH . "/${resource}";
		}
		fatal("Unknown SSL extension `%s'", $ext);
		return '';
	}
}