<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2018
	 */

	namespace Opcenter\Dns;

	use Illuminate\Contracts\Support\Arrayable;

	class Record implements Arrayable, \ArrayAccess
	{
		// @var array additional records that require TLC
		const FORMATTED_RECORDS = ['MX', 'SRV', 'CAA', 'CERT', 'DNSKEY', 'DS', 'LOC',
			'NAPTR', 'SMIMEA', 'SOA', 'SSHFP', 'TLSA', 'URI'];
		// @var string dns zone name
		protected $zone;
		// @var string record name
		protected $name;
		// @var string rr type
		protected $rr;
		// @var string dns parameter
		protected $parameter;
		// @var int record TTL
		protected $ttl;
		// @var string class
		protected $class = 'IN';

		//@var array internal meta
		protected $meta;
		protected bool $initialized = false;

		public function __construct(string $zone, array $args)
		{
			foreach ($args as $k => $v) {
				if (!property_exists($this, $k)) {
					fatal("unknown DNS record property specified `%s'", $k);
				}
				if ($k === 'rr') {
					$v = strtoupper($v);
				}
				if ($v !== NULL && $k === 'ttl') {
					$v = (int)$v;
				} else if ($k === 'name' && $v === '@') {
					// strip origin marker
					$v = '';
				}
				$this->$k = $v;
			}
			$this->rr = strtoupper($this->rr);
			if (\in_array($this->rr, self::FORMATTED_RECORDS)) {
				$this->format();
			}

			$this->postFormat();
			$this->zone = $zone;

			$this->initialized = true;
		}

		/**
		 * Store additional meta specific to record
		 */
		protected function format()
		{
			if (!$this->parameter && $this->rr == 'MX') {
				return error('MX record requires a parameter');
			}

			$keys = $this->getMetaFromRr($this->rr);
			if ($this->rr === 'SRV') {
				$components = array_pad(explode('.', $this->name, 3), 3, '');
				for ($i = 0; $i < 3; $i++) {
					$k = array_shift($keys);
					$v = array_shift($components);
					$this->setMeta($k, $v);
				}
			}

			$count = \count($keys);
			if (null === $this->parameter) {
				return;
			}

			$params = preg_split('/\s+/', $this->parameter, $count);
			if ($count !== \count($params)) {
				return error('RR %s is missing parameters. Got %d, expected %d', $this->rr, \count($params), $count);
			}
			foreach (array_combine($keys, $params) as $k => $v) {
				$this->setMeta($k, $v);
			}
		}

		/**
		 * Format parameter
		 */
		protected function postFormat(): void
		{
			// post-processing
			$fn = 'format' . ucwords($this->rr);
			if (method_exists($this, $fn)) {
				$this->{$fn}();
			}
		}

		/**
		 * Update parameter from meta
		 *
		 * @return string formatted parameter
		 */
		private function updateParameter(): string
		{
			if (!\in_array($this->rr, static::FORMATTED_RECORDS, true)) {
				return $this->parameter;
			}

			$this->parameter = $this->parameterFromMeta();

			$this->postFormat();
			return $this->parameter;
		}

		/**
		 * Get meta tags from RR
		 *
		 * @param string $rr
		 * @return array
		 */
		protected function getMetaFromRr(string $rr): array
		{
			switch (strtoupper($rr)) {
				case 'A':
				case 'AAAA':
				case 'TXT':
				case 'PTR':
					return [];
				case 'MX':
					return ['priority', 'data'];
				case 'SRV':
					return ['service', 'protocol', 'name', 'priority', 'weight', 'port', 'data'];
				case 'CAA':
					return ['flags', 'tag', 'data'];
				case 'CERT':
					return ['type', 'key_tag', 'algorithm', 'data'];
				case 'DNSKEY':
					return ['flags', 'protocol', 'algorithm', 'data'];
				case 'DS':
					return ['key_tag', 'algorithm', 'digest_type', 'data'];
				case 'NAPTR':
					return ['order', 'preference', 'flags', 'service', 'regex', 'data'];
				case 'SMIMEA':
					return ['usage', 'selector', 'matching_type', 'data'];
				case 'SOA':
					return ['mname', 'rname', 'serial', 'refresh', 'retry', 'expire', 'ttl'];
				case 'SSHFP':
					return ['algorithm', 'type', 'data'];
				case 'TLSA':
					return ['usage', 'selector', 'matching_type', 'data'];
				case 'URI':
					return ['priority', 'weight', 'data'];
				case 'LOC':
					return [
						'lat_degrees',
						'lat_minutes',
						'lat_seconds',
						'lat_direction',
						'long_degrees',
						'long_minutes',
						'long_seconds',
						'long_direction',
						'altitude',
						'size',
						'precision_horz',
						'precision_vert'
					];
			}
			return [];
		}

		public function __debugInfo()
		{
			return $this->toArray() + [
				'id' => $this->getMeta('id')
			];
		}

		/**
		 * Get record meta
		 *
		 * @param string $key key
		 * @return mixed|null
		 */
		public function getMeta(string $key)
		{
			return $this->meta[$key] ?? null;
		}

		/**
		 * Set record meta
		 *
		 * @param mixed $key
		 * @param mixed|null $value
		 * @return self
		 */
		public function setMeta($key, $value = null): self
		{
			if (\is_array($key)) {
				$this->meta = array_replace($this->meta, $key);
			} else if ($value === null) {
				array_forget($this->meta, $key);
			} else {
				array_set($this->meta, $key, $value);
			}

			if ($this->initialized) {
				$this->updateParameter();
			}

			return $this;
		}

		public function is(Record $r): bool
		{
			if ($r->getZone() !== $this->zone) {
				fatal("Sanity check failed - matching zone `%s' does not match requested zone `%s' - validate logic",
					$this->zone, $r->getZone());
			}
			if ($this->rr !== strtoupper($r['rr'])) {
				return false;
			}
			if ($r['name'] === '@') {
				if ($this->name && $this->name !== $r['name']) {
					return false;
				}
			} else if ($this->name !== $r['name']) {
				return false;
			}

			if (!empty($r['ttl']) && $r['ttl'] !== $this->ttl) {
				return false;
			}

			if (empty($r['parameter'])) {
				return true;
			}

			if ($this->rr === 'LOC') {
				$a = explode(' ', $this->parameter);
				foreach (explode(' ', $r['parameter']) as $b) {
					// weak typing, 0 => 0.000 and so on
					$tmp = current($a);
					if (0 !== ($pos = strspn($tmp, '0123456789.'))) {
						$pos2 = strspn($b, '0123456789.');
						// 0.00m == 0m
						if (substr($tmp, $pos) !== substr($b, $pos2)) {
							// 0.00 != 0
							return false;
						}
						if (substr($tmp, 0, $pos) != substr($b, 0, $pos2)) {
							// m != m
							return false;
						}
					} else if ($b != $tmp) {
						return false;
					}
					next($a);
				}
				return current($a) === false;
			} else if ($this->rr === 'SSHFP') {
				return strtolower($this->parameter) === strtolower($r['parameter']);
			} else if ($this->rr === 'MX') {
				// some DNS providers are more pedantic than others
				return rtrim($this->parameter, '.') === rtrim($r['parameter'], '.');
			}

			$meta = $this->getMetaFromRr($this->rr);

			if (!$meta) {
				return $r['parameter'] === $this->parameter;
			}

			foreach ($meta as $k) {
				$v = $this->getMeta($k);
				if (empty($v)) {
					continue;
				}
				if ($k === 'data') {
					$data = $r->getMeta($k);
					if ($this->trim($v) !== $this->trim($data)) {
						// take some guesswork out of parameters
						// from the API and internally stored
						return false;
					}
				} else if ($r->getMeta($k) != $v) {
					return false;
				}
			}

			return true;
		}

		/**
		 * Trim quotes from parameter
		 *
		 * @param string $data
		 * @return string
		 */
		protected function trim(string $data): string
		{
			if (isset($data[1]) && $data[0] === $data[-1] && $data[0] === '"') {
				return substr($data, 1, -1);
			}

			return $data;
		}

		/**
		 * Requested record field matches
		 *
		 * @param string $field    field to match
		 * @param        $against  data to match against
		 * @return bool
		 */
		public function matches(string $field, $against): bool
		{
			if ($field !== 'hostname' && !property_exists($this, $field)) {
				fatal("Unknown property `%s'", $field);
			}
			if ($field === 'rr') {
				$against = strtoupper($against);
			} else if ($field === 'hostname') {
				return rtrim((string)$against, '.') === ltrim($this->name . '.' . $this->zone, '.');
			}
			return $against === $this->{$field};
		}

		public function getZone(): string
		{
			return $this->zone;
		}

		public function __toString()
		{
			extract($this->toArray(), EXTR_OVERWRITE);

			return $this->hostname() . ".\t${ttl}\t${class}\t${rr}\t${parameter}";
		}

		public function toArray(): array
		{
			return [
				'zone'      => $this->zone,
				'name'      => $this->name,
				'rr'        => $this->rr,
				'ttl'       => $this->ttl,
				'parameter' => $this->parameter,
				'class'     => 'IN',
			];
		}

		/**
		 * Record hostname
		 *
		 * @return string
		 */
		public function hostname(): string
		{
			return ltrim(implode('.', [$this->name, $this->zone]), '.');
		}

		public function offsetExists($offset): bool
		{
			return isset($this->{$offset});
		}

		public function &offsetGet($offset)
		{
			return $this->{$offset};
		}

		public function offsetSet($offset, $value): void
		{
			if (!property_exists($this, $offset)) {
				fatal("unknown DNS property `%s'", $offset);
			}
			$this->{$offset} = $value;
		}

		public function offsetUnset($offset): void
		{
			if (!isset($this->{$offset})) {
				return;
			}
			unset ($this->{$offset});
		}

		/**
		 * Update properties with new properties
		 *
		 * @param Record $new
		 * @return Record
		 */
		public function merge(Record $new): self
		{
			foreach (array_keys(get_object_vars($this)) as $k) {
				if (isset($new[$k])) {
					$this->$k = $new[$k];
				}
			}

			return $this;
		}

		public function parameterFromMeta(): string
		{
			if (!\in_array($this->rr, self::FORMATTED_RECORDS, true)) {
				return $this['parameter'];
			}

			return implode(' ', array_intersect_key(
				$this->meta, array_flip($this->getMetaFromRr($this->rr))
			));
		}
	}