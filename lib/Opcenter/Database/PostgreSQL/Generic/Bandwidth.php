<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Database\PostgreSQL\Generic;

	class Bandwidth
	{
		public function createBandwidthSpan(int $site_id)
		{
			return "INSERT INTO bandwidth_spans (site_id, begindate) VALUES (${site_id}, NOW())";
		}

		/**
		 * Remove bandwidth span data  for site
		 *
		 * @param int            $site_id
		 * @param \DateTime|null $begin
		 * @return string
		 */
		public function deleteBandwidthSpan(int $site_id, ?\DateTime $begin)
		{
			$query = 'DELETE FROM bandwidth_spans WHERE site_id = ' . $site_id;
			if ($begin) {
				$query .= ' AND begindate = TO_TIMESTAMP(' . $begin->getTimestamp() . ')::date';
			}

			return $query;
		}

		/**
		 * Delete extended bandwidth data from database
		 *
		 * @param int      $site_id
		 * @param int|null $svc_id
		 * @return string
		 */
		public function deleteExtendedBandwidth(int $site_id, ?int $svc_id): string
		{
			$q = 'DELETE FROM bandwidth_extendedinfo USING bandwidth_services WHERE ' .
				'bandwidth_extendedinfo.svc_id = bandwidth_services.svc_id AND ' .
				'bandwidth_extendedinfo.site_id = ' . $site_id;
			if ($svc_id) {
				$q .= " AND bandwidth_bandwidth_services.svc_id = ${svc_id} AND " .
					'bandwidth_extendedinfo.svc_id = bandwidth_log.svc_id';
			}

			return $q;
		}
	}