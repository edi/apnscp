<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */


	namespace Opcenter\Net\Firewall;

	use Opcenter\Contracts\VirtualizedContextable;
	use Opcenter\Map;
	use Opcenter\Net\Ip6;
	use Opcenter\Service\Validators\Rampart\Whitelist;

	/**
	 * Class Firewall
	 *
	 * A simple firewall to block incoming connections
	 *
	 * @package Opcenter\Net
	 */
	class Delegated implements VirtualizedContextable
	{
		const IPSET_NAME = RAMPART_DELEGATION_SET;

		use \apnscpFunctionInterceptorTrait;
		use \AccountInfoTrait {
			instantiateContexted as private instantiateContextedReal;
		}

		/**
		 * @var \Auth_Info_User
		 */
		protected $context;
		/**
		 * @var \apnscpFunctionInterceptor
		 */
		protected $afi;

		private function __construct() {}

		public static function instantiateContexted(\Auth_Info_User $context)
		{
			return static::instantiateContextedReal($context);
		}

		/**
		 * Delegation is permitted
		 *
		 * @return bool
		 */
		public function permitted(): bool
		{
			// @TODO handling of new service injection defaults, check .skeleton?
			return \Rampart_Module::FAIL2BAN_DRIVER === 'ipset' && $this->getServiceValue('rampart', 'enabled', true) &&
				$this->getServiceValue('rampart', 'max', RAMPART_DELEGATED_WHITELIST) !== 0;
		}

		/**
		 * Get delegated entries
		 *
		 * @return array
		 */
		public function get(): array
		{
			$value = (array)$this->getServiceValue('rampart', 'whitelist', []);
			$ctr = 0;
			// IPv6 fixes
			$value = array_key_map(static function ($k, $v) use (&$ctr) {
				if ($k !== $ctr++ && false !== strpos($v, ':')) {
					return "$k:$v";
				}
				return $v;
			}, $value);
			return $value;
		}

		/**
		 * Create new delegated entry
		 *
		 * @param string $ip
		 * @return bool
		 */
		public function add(string $ip): bool
		{
			if (!$this->permitted()) {
				return error('Whitelisting not enabled for account');
			}
			$ips = $this->get();
			if ($this->exists($ip)) {
				return warn("IP address `%s' is already whitelisted", $ip);
			}
			$ips[] = $ip;
			$editor = new \Util_Account_Editor($this->getAuthContext()->getAccount(), $this->getAuthContext());
			$editor->setConfig('rampart', 'whitelist', $ips);
			return $editor->edit();

		}

		/**
		 * Remove IP address from delegation
		 *
		 * @param string $ip
		 * @return bool
		 */
		public function remove(string $ip): bool
		{
			if (!$this->permitted()) {
				return error('Whitelisting not enabled for account');
			}
			if (!$this->exists($ip)) {
				return error("IP address `%s' is not present in delegated whitelist", $ip);
			}
			$ips = $this->get();
			$ips = array_diff($ips, [$ip]);
			$editor = new \Util_Account_Editor($this->getAuthContext()->getAccount(), $this->getAuthContext());
			$editor->setConfig('rampart', 'whitelist', $ips);
			return $editor->edit();
		}

		/**
		 * IP exists in delegated whitelist
		 *
		 * @param string $ip
		 * @return bool
		 */
		public function exists(string $ip): bool
		{
			$ips = $this->get();
			return \in_array($ip, $ips, true);
		}

		/**
		 * Get all sites authorizing IP address
		 *
		 * @param string $ip
		 * @return array
		 */
		public static function getDelegators(string $ip): array
		{
			$map = Map::load(Whitelist::MAP_FILE, 'c');
			return array_filter(explode(' ', (string)$map->fetch($ip)));
		}

		/**
		 * Claim IP delegation by marker
		 *
		 * @param string $ip delegatee
		 * @param string $marker delegator
		 * @return bool
		 */
		public static function markDelegated(string $ip, string $marker): bool
		{
			$entries = static::getDelegators($ip);
			if (!\in_array($marker, $entries, true)) {
				$entries[] = $marker;
			}
			$map = Map::load(Whitelist::MAP_FILE, 'c');
			return $map->set($ip, implode(' ', $entries));
		}

		/**
		 * Release delegation claim
		 *
		 * @param string $ip delegatee
		 * @param string $marker delegator
		 * @return bool
		 */
		public static function releaseDelegation(string $ip, string $marker): bool
		{
			$entries = array_diff(static::getDelegators($ip), [$marker]);
			$map = Map::load(Whitelist::MAP_FILE, 'c');
			if (empty($entries)) {
				return $map->delete($ip);
			}
			return $map->set($ip, implode(' ', $entries));
		}

	}