<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Migration;

	use Opcenter\Service\Plans;

	class CliParser
	{
		/**
		 * Parse CLI arguments
		 *
		 * @param string $mode mode one of 'import', 'export', 'transfer'
		 * @return array
		 */
		public static function parse(string $mode): array
		{
			$options = [];
			if ($_SERVER['argc'] < 2) {
				static::help($mode);
			}

			$optind = null;
			$shortOpts = 'f:o:hp:Vc:dn';
			$longOpts = [
				'no-scan',
				'no-create',
				'no-activate',
				'no-builtin',
				'late-quota',
				'late-quotas',
				'format:',
				'help',
				'output:',
				'plan:',
				'version',
				'dump',
				'dry-run',
				'delete',
				'unsafe-sources',
				'no-bootstrap'
			];
			if ($mode === 'import' || $mode === 'transfer') {
				$longOpts[] = 'delete';
				$longOpts[] = 'drop-forwarded-catchalls';
				$longOpts[] = 'apply-fortification:';
				$longOpts[] = 'conflict:';
				$longOpts[] = 'reset';
				$longOpts[] = 'no-dns';
			}
			foreach (getopt($shortOpts, $longOpts, $optind) as $opt => $val)
			{
				switch ($opt) {
					case 'no-create':
						$options['no-create'] = true;
						break;
					case 'no-activate':
						$options['no-activate'] = true;
						break;
					case 'no-scan':
						$options['no-scan'] = true;
						break;
					case 'no-builtin':
						$options['no-builtin'] = true;
						break;
					case 'apply-fortification':
						if (!\in_array($val, ['max', 'min', 'release'], true)) {
							fatal("unknown Fortification profile `%s' selected", $val);
						}
						$options['apply-fortification'] = $val;
						break;
					case 'V':
					case 'version':
						echo \Opcenter::version(), PHP_EOL, PHP_EOL;
						exit(0);
					case 'help':
					case 'h':
						static::help($mode);
					case 'output':
					case 'o':
						if ($val !== 'json' && $val !== 'text') {
							fatal("Unknown output type: `%s'", $val);
						}
						if ($val === 'json') {
							\Error_Reporter::set_verbose(0);
							register_shutdown_function(static function () {
								return register_shutdown_function(static function () {
									print json_encode(\Error_Reporter::get_buffer());
									exit((int)\Error_Reporter::is_error());
								});
							});
						}
						$options['output'] = $val;
						break;
					case 'p':
					case 'plan':
						if (!Plans::exists($val)) {
							fatal("Unknown plan `%s'", $val);
						}
						$options['plan'] = $val;
						break;
					case 'f':
					case 'format':
						if (!Format::formatValidForTask($val, $mode)) {
							fatal("Unknown %s format type `%s'", $mode, $val);
						}
						$options['format'] = $val;
						break;
					case 'd':
					case 'dump':
						$options['dump'] = true;
						break;
					case 'c':
						$options = array_merge_recursive(
							$options, [
							'conf' => \Opcenter\CliParser::parseServiceConfiguration($val)
						]);
						break;
					case 'delete':
						$options['delete'] = true;
						break;
					case 'conflict':
						if (!\in_array($val, Remediator::list(), true)) {
							fatal("Unknown conflict strategy `%s'", $val);
						}
						$options['conflict'] = $val;
						break;
					case 'drop-forwarded-catchalls':
						$options['drop-forwarded-catchalls'] = true;
						break;
					case 'late-quota':
					case 'late-quotas':
						$options['late-quota'] = true;
						break;
					case 'reset':
						$options['reset'] = true;
						break;
					case 'dry-run':
					case 'n':
						$options['dry-run'] = true;
						$options['dump'] = true;
						break;
					case 'unsafe-sources':
						$options['unsafe'] = true;
						break;
					case 'no-bootstrap':
						$options['no-bootstrap'] = true;
						break;
					case 'no-dns':
						$options['no-dns'] = true;
						break;
					case '--':
						break;
					default:
						fatal("Unknown option `%s'", $opt);
				}
			}
			$command = \array_slice($_SERVER['argv'], $optind);

			return [
				'options' => $options,
				'command' => $command
			];
		}

		/**
		 * Display help
		 *
		 * @param string $mode migration mode
		 * @return void
		 */
		public static function help(string $mode): void
		{
			$flatten = static function ($var) {
				if (\is_array($var)) {
					return '[' . implode(',', $var) . ']';
				}

				return $var;
			};
			echo \Opcenter::version(), PHP_EOL;
			$help = \Opcenter\CliParser::getHelpFromModules();
			ksort($help);
			foreach ($help as $module => $vars) {
				printf("%s module\n", $module);
				printf("%s\n", str_repeat('=', 80));
				foreach ($vars as $var => $info) {
					$default = '[default: ' . $flatten($info['default']) . ']';
					printf("    %-12s: %-16s %s %s\n", $var, $default, $flatten($info['range']), $info['help']);
				}
				printf("\n\n");
			}
			$formatTypes = BaseMigration::appendNamespace(ucwords($mode))::getFormats();
			echo "Non-module options:\n" .
				str_repeat('=', 80) . "\n" .
				"-c service,param=val\t\toverride creation parameters\n" .
				"-o, --output=[json|text]\toutput normally in text or json (stdout)\n" .
				'-f, --format=[' . implode('|', $formatTypes) . "]\tformat type\n" .
				"-d, --dump\t\t\texport metadata in yaml\n" .
				"-n, --dry-run\t\t\tshow what will be done (see Migrations.md)\n" .
				"--unsafe-sources\t\timport unsafe data (webmail profiles)\n" .
				// @TODO update for migration modes
				"--delete\t\t\tdelete backup upon completion\n" .
				"--no-builtin\t\t\tuse tar for archive handling instead of builtin\n" .
				'--conflict=[' . implode('|', Remediator::list()) . "]\temail conflict resolution\n" .
				"--no-create \t\t\tdisable account creation\n" .
				"--drop-forwarded-catchalls \twarn and continue if forwarded catch-all\n" .
				"--apply-fortification=[max,min,release]\tset Fortification profile for detected webapps\n" .
				"--late-quota\t\t\tgrant storage amnesty during import\n" .
				"--no-scan \t\t\tdisable Web App intake\n" .
				"--no-bootstrap\t\t\tdisable SSL bootstrapping\n" .
				"--no-dns\t\t\tdisable reading DNS records\n" .
				"--no-activate \t\t\tdisable IP address change on import\n\n";

			exit(1);
		}
	}

