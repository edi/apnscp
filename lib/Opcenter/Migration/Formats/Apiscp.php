<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Migration\Formats;

	use Opcenter\Account\Create;
	use Opcenter\Filesystem;
	use Opcenter\Migration\Bill;
	use Opcenter\Migration\Contracts\ImportInterface;
	use Opcenter\Migration\FormatType;
	use Opcenter\SiteConfiguration;

	class Apiscp extends FormatType implements ImportInterface
	{
		/**
		 * @var array
		 */
		protected $cfg;

		/**
		 * File can be processed as backup
		 *
		 * @return bool
		 */
		public function check(): bool
		{

		}

		public function run(): bool
		{
			$stream = $this->getStream();

			// domain is resolved later
			$baseDir = $stream->getPrefix();
			$this->cfg = $this->readConfiguration($baseDir . '/info/current');
			return true;
		}

		private function readConfiguration(string $path) {
			$cfg = [];
			$cfg['siteinfo'] = \Util_Conf::parse_ini($path . DIRECTORY_SEPARATOR . '/siteinfo');
			$this->bill = new Bill(array_get($cfg, 'siteinfo.domain'));
			Filesystem::readdir($path, function ($f) use (&$cfg, $path) {
				$cfg[$f] = \Util_Conf::parse_ini($path . DIRECTORY_SEPARATOR . $f);
				foreach ($cfg[$f] as $param => $val) {
					$this->bill->set($f, $param, $val);
				}
			});

			return $cfg;
		}
	}