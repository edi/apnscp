<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */


	namespace Opcenter\System\Cgroup;

	use Opcenter\System\Cgroup\Contracts\Renderable;

	class Attribute implements Renderable
	{

		public function __construct(string $attribute, $value)
		{

		}

		public function build(): string
		{
			return $this->attribute;
		}

	}