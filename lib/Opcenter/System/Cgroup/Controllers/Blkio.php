<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

	namespace Opcenter\System\Cgroup\Controllers;

	use Opcenter\System\Cgroup\Attributes\Blkio\Weight;
	use Opcenter\System\Cgroup\Contracts\ControllerAttribute;
	use Opcenter\System\Cgroup\Controller;

	class Blkio extends Controller
	{
		// @var \Auth_Info_User $ctx
		private $ctx;

		protected const ATTRIBUTES = [
			'writeiops' => 'blkio.throttle.write_iops_device',
			'readiops'  => 'blkio.throttle.read_iops_device',
			'writebw'   => 'blkio.throttle.write_bps_device',
			'readbw'    => 'blkio.throttle.read_bps_device',
			'ioweight'  => 'blkio.weight'
		];

		/**
		 * @var array metric counters for blkio
		 * NOTE: fields are capitalized to reflect formatting in counter
		 */
		public const LOGGABLE_METRICS = [
			'bw-read' => [
				'label'   => 'Read (KB)',
				'type'    => 'monotonic',
				'counter' => 'blkio.throttle.io_service_bytes:Read'
			],
			'bw-write' => [
				'label'   => 'Write (KB)',
				'type'    => 'monotonic',
				'counter' => 'blkio.throttle.io_service_bytes:Write'
			],
			'bk-read' => [
				'label'   => 'Read (blocks)',
				'type'    => 'monotonic',
				'counter' => 'blkio.throttle.io_serviced:Read'
			],
			'bk-write' => [
				'label'   => 'Write (blocks)',
				'type'    => 'monotonic',
				'counter' => 'blkio.throttle.io_serviced:Write'
			],
		];

		public function import(\Auth_Info_User $ctx = null): int
		{
			$this->ctx = $ctx ?? \Auth::profile();
			return parent::import($ctx);
		}

		public function setAttribute(ControllerAttribute $attr): bool
		{
			$attr->setContext($this->ctx);
			if (\get_class($attr) === Weight::class && !$attr->canUse()) {
				debug('Skipping weight - non CFQ/BFQ elevator detected');
				return false;
			}

			$this->dirty = true;
			$this->attributes[(string)$attr] = $attr;

			return true;
		}

		protected function readRawCounter(string $path): string
		{
			$delim = strrpos($path, ':');
			$type = substr($path, $delim+1);
			$path = substr($path, 0, $delim);
			$raw = parent::readRawCounter($path);
			$str = strtok($raw, "\n");
			do {
				if (false === strpos($str, " $type ")) {
					continue;
				}
				$counter = substr($str, strrpos($str, ' '));
				if (substr($path, -6) !== '_bytes') {
					return $counter;
				}
				return (string)((int)$counter/1024);
			} while (false !== ($str = strtok("\n")));
			debug("Unknown/invalid blkio counter attr `%s' in %s", $type, $path);
			return '0';
		}


	}
