<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2021
 */

namespace Opcenter\System;

use Opcenter\Admin\Bootstrapper\Config;
use Opcenter\Admin\Settings\Cp\Bootstrapper;
use Opcenter\Map;

class Procfs
{
	const PROC_HOME = '/proc/sys';

	/**
	 * Read procfs key
	 *
	 * @param string $key
	 */
	public static function read(string $key)
	{
		$path = static::makePath($key);
		if (!file_exists($path)) {
			return null;
		}

		return \Util_Conf::inferType(trim(file_get_contents($path)));
	}

	/**
	 * Write key temporarily
	 *
	 * @param string $key
	 * @param        $val
	 * @return bool
	 */
	public static function write(string $key, $val): bool
	{
		$path = static::makePath($key);
		if (!file_exists($path)) {
			return error("Key `%s' does not exist in procfs", $key);
		}

		return file_put_contents($path, $val) > 0 ?:
			error("Write failed to `%s'", $key);
	}

	/**
	 * Persist changes in configuration
	 *
	 * @param string $key
	 * @param        $val
	 * @return bool
	 */
	public static function persist(string $key, $val): bool
	{
		if (!static::write($key, $val)) {
			return error("Failed to write value `%s'", $key);
		}

		$key = str_replace('/', '.', $key);
		if (!Map::load(self::sysctlConfig(), 'w')->set($key, (string)$val)) {
			return error("Failed writing sysctl configuration");
		}
		$cfg = (new Bootstrapper());
		return $cfg->set('sysctl_custom_config', [$key => $val] + $cfg->get('sysctl_custom_config'));
	}

	/**
	 * ApisCP sysctl configuration
	 *
	 * @return string
	 */
	private static function sysctlConfig(): string
	{
		$cfg = (new Config())->loadRole('system/sysctl');
		return $cfg['sysctl_file'];
	}

	/**
	 * procfs key exists
	 *
	 * @param string $key
	 * @return bool
	 */
	public static function exists(string $key): bool
	{
		$path = static::makePath($key);
		return file_exists($path);
	}

	/**
	 * Make path from key
	 *
	 * @param string $key period delimited key
	 * @return string
	 */
	private static function makePath(string $key): string
	{
		return self::PROC_HOME . '/' . str_replace('.', '/', $key);
	}
}