<?php

namespace Lararia\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class MakeScopeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:scope 
                                {scope : Scope to locate command under} 
                                {name  : Command name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new Scope';

    protected $type = 'scope';

	public function handle()
	{
		if (!parent::handle()) {
			return false;
		}
		$cache = \Cache_Global::spawn();
		$cache->del(\Scope_Module::SCOPE_CACHE_KEY);
	}


	/**
	 * Replace the class name for the given stub.
	 *
	 * @param string $stub
	 * @param string $name
	 * @return string
	 */
	protected function replaceClass($stub, $name)
	{
		return str_replace('DummyClass', studly_case($this->getCommandName()), $stub);
	}

	/**
	 * Get the stub file for the generator.
	 *
	 * @return string
	 */
	protected function getStub()
	{
		return resource_path('storehouse/stubs/scope-command.stub');
	}

	/**
	 * @inheritDoc
	 */
	protected function qualifyClass($name)
	{
		$name = studly_case($name);
		return parent::qualifyClass($name);
	}

	protected function rootNamespace()
	{
		return 'Opcenter\\';
	}

	/**
	 * Get the destination class path.
	 *
	 * @param string $name
	 * @return string
	 */
	protected function getPath($name)
	{
		if (getenv('MAINLINE')) {
			// official Scope generation
			return INCLUDE_PATH . '/lib/' . str_replace('\\', '/', $name) . '.php';
		}
		return config_path('custom/scopes/' . str_replace('\\', '/', $name) . '.php');
	}

	/**
	 * Get the default namespace for the class.
	 *
	 * @param string $rootNamespace
	 * @return string
	 */
	protected function getDefaultNamespace($rootNamespace)
	{
		return $rootNamespace . '\\Admin\\Settings\\' . studly_case($this->getScopeName());
	}

	protected function getScopeName(): string
	{
		return $this->argument('scope');
	}

	protected function getCommandName(): string
	{
		return $this->argument('name');
	}

}
