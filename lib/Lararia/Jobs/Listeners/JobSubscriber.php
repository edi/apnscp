<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs\Listeners;

	use Illuminate\Queue\Events\JobProcessed;
	use Lararia\Jobs\Traits\RawManipulation;
	use Laravel\Horizon\Events\JobFailed;

	class JobSubscriber
	{
		use RawManipulation;

		public function subscribe($events)
		{
			$events->listen(
				JobProcessed::class,
				self::class . '@jobFinished'
			);

			$events->listen(
				JobFailed::class,
				self::class . '@jobFailed'
			);
		}

		public function jobFailed(JobFailed $event)
		{
		}

		public function jobFinished(JobProcessed $event)
		{

		}

		protected function notify(\Lararia\Jobs\Job $job)
		{

		}
	}