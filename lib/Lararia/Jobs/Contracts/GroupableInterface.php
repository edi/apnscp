<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs\Contracts;

	/**
	 * Interface GroupableInterface
	 *
	 * Batching tasks by a common tag.
	 * Notification only dispatches once group is completed.
	 *
	 * @package Lararia\Jobs\Contracts
	 */
	interface GroupableInterface
	{

	}

