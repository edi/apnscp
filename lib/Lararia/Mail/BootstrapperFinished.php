<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Mail;

	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;
	use Lararia\Jobs\Job;

	class BootstrapperFinished extends Mailable
	{
		use SerializesModels;
		protected $job;
		protected $stats;

		/**
		 * Create a new message instance.
		 *
		 * @param Job\Report $report
		 */
		public function __construct(Job\Report $report)
		{
			$this->job = $report->current();
			$this->stats = new Job\Reports\BootstrapperTask($this->job->getTimingLog());
		}

		protected function getStats(): Job\Reports\BootstrapperTask {
			return $this->stats;
		}

		public function build()
		{
			$buffer = implode("\n", \Error_Reporter::format_buffer());
			$msg = $this->markdown('email.admin.bootstrapper', [
				'job' => $this->job,
				'stats' => $this->getStats()
			])->subject('Bootstrapper task finished')
				->to($this->job->common_get_email())
				->attach($this->job->getTee(), ['as' => 'bootstrapper-log.txt']);
			if ($buffer) {
				$msg->attachData($buffer, 'debug-log.txt');
			}

			return $msg;
		}
	}