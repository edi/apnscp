<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	include_once('tar.php');

	/**
	 * Provides bzip compression/decompression routines in the file manager
	 *
	 * @package Compression
	 */
	class Bzip_Filter extends Tar_Filter
	{
		public static function extract_files($archive, $destination, array $files = null, array $opts = null)
		{
			$ext = self::$fc->compression_extension($archive);
			$tar = in_array($ext, array('.tbz', '.tbz2', '.tar.bz', '.tar.bz2'));
			$cmd = 'bzip2 ' . (!$tar ? '-d' : '-d -c') . ' %s';
			$proc = parent::exec(
				$cmd,
				$archive,
				array(0),
				array('run' => !$tar)
			);

			if ($tar) {
				return self::extract_files_pipe($archive, $destination, $files, $opts);
			}
			if (!$proc['success']) {
				return false;
			}

			self::$fc->file_delete($archive);

			return true;
		}


		public static function list_files($archive, array $opts = null)
		{
			$ext = self::$fc->compression_extension($archive);
			$tar = in_array($ext, array('.tbz', '.tbz2', '.tar.bz', '.tar.bz2'));
			if ($tar) {
				$cmd = 'bzip2 -d -c %s';
				$proc = parent::exec(
					$cmd,
					$archive,
					array(0),
					array('run' => 0)
				);

				return self::list_files_pipe($archive,
					$opts
				);
			}

			$files = array();
			$files[] = array(
				'file_name'   => basename($archive, self::$fc->compression_extension($archive)),
				'file_type'   => 'file',
				'can_read'    => true,
				'can_write'   => true,
				'can_execute' => true,
				'size'        => strlen(file_get_contents('compress.bzip2://' . $archive)),
				'packed_size' => filesize($archive),
				'crc'         => '?',
				'link'        => 0,
				'date'        => filectime($archive)
			);

			return $files;

		}

	}

?>
