<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	abstract class Archive_Base implements IArchive
	{
		protected static $fc;
		protected static $binary_handler;
		protected static $mapping_list;
		private $files;

		public static function add_files($mArchive, $mFile)
		{
			return error('add: method not implemented');
		}

		public static function create_archive($mArchive, $mFiles)
		{
			return error('create: method not implemented');
		}

		public static function delete_files($mArchive, $mFile)
		{
			return error('delete: method not implemented');
		}

		public static function stat($mArchive)
		{
			return array();
		}

		static public function init($fc)
		{
			self::$fc = $fc;
		}

		protected static function exec($cmd, $args)
		{
			return call_user_func_array('Util_Process_Safe::exec', func_get_args());
		}

		protected function add_file($name, array $stat = array())
		{
			$files[] = $name;
		}

	}

?>
