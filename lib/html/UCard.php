<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	use Opcenter\Map;
	use Opcenter\Service\Validators\Billing\Invoice;
	use Opcenter\Service\Validators\Billing\ParentInvoice;

	/**
	 * UI session control
	 *
	 */
	class Credentials
	{
		public $user;
		public $domain;
		public $level;
		public $site = null;
		public $site_id = null;
		public $avatar;
		public $name;
		public $impersonableDomains = [];
	}

	class UCard
	{
		use apnscpFunctionInterceptorTrait;
		use ContextableTrait;

		private static $_reloadFlag = false;
		private static $instances = array();
		private static $role_map = array(
			'admin'    => PRIVILEGE_ADMIN,
			'reseller' => PRIVILEGE_RESELLER,
			'site'     => PRIVILEGE_SITE,
			'user'     => PRIVILEGE_USER
		);
		private static $active;
		private static $saveHook = false;
		protected $_gravatar;
		private $card;
		private $_cgroups;

		private function __construct()
		{

			if (!\Auth::authenticated()) {
				return;
			}
			// @todo not multiuser friendly
			$this->card = new Credentials();
			$auth = $this->getAuthContext();
			$this->card->user = $auth->username;
			$this->card->domain = $auth->domain;
			$this->card->level = $auth->level;
			if ($this->card->level & (PRIVILEGE_SITE | PRIVILEGE_USER)) {
				$this->card->site_id = $auth->site_id;
				$this->card->site = $auth->site;
			}
			$this->card->impersonableDomains = [];
			if ( ($this->card->level & PRIVILEGE_SITE) && AUTH_SUBORDINATE_SITE_SSO &&
				($this->common_get_service_value('billing', 'invoice')))
			{
				$sites = Map::load(ParentInvoice::MAP_FILE, 'r-')->fetchAll();
				$this->card->impersonableDomains = array_map(static function ($site) {
					return \Auth::get_domain_from_site_id($site);
				}, array_keys(array_filter($sites, function ($site) {
					return $site === $this->card->site;
				})));
			}

			$this->card->name = $this->loadGravatar('displayName');
			if (STYLE_GRAVATAR) {
				$this->card->avatar = 'https://secure.gravatar.com/avatar/' . $this->loadGravatar('hash') . '?d=' . urlencode(STYLE_GRAVATAR);

			}
		}

		protected function loadGravatar($spec)
		{
			if (null !== $this->_gravatar) {
				return array_get($this->_gravatar, $spec);
			}
			if (null !== ($avatar = Session::get('gravatar'))) {
				$this->_gravatar = $avatar;

				return array_get($avatar, $spec);
			}

			$email = $this->common_get_email();

			$hash = md5(strtolower($email));
			$resp = silence(static function () use ($hash) {
				$agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : PANEL_BRAND . ' v' . APNSCP_VERSION;
				$context = stream_context_create([
					'http' => [
						'header' => 'User-Agent: ' . $agent
					]
				]);

				return file_get_contents('https://en.gravatar.com/a' . $hash . '.json', null, $context);
			});
			$data = (array)json_decode($resp, true);
			$data = !empty($data['entry']) ? $data['entry'][0] : [];
			$data['hash'] = $hash;
			$this->_gravatar = $data;
			Session::set('gravatar', $this->_gravatar);

			return array_get($this->_gravatar, $spec, null);
		}

		/**
		 * Force reinitialization of
		 *
		 * Used when changing email to force reinit of avatar
		 */
		public static function reinit()
		{
			self::$active = null;
			self::$instances = null;
		}

		public static function is(...$role)
		{
			return call_user_func_array([static::get(), 'hasPrivilege'], $role);
		}

		public static function get(): self
		{
			if (null === self::$active) {
				if (null !== ($card = Session::get('ucard')) ) {
					self::$active = $card;
				} else {
					$card = self::init();
					Session::set('ucard', $card);
				}
			}

			return self::$active;
		}

		public static function init(\Auth_Info_User $ctx = null): UCard
		{
			$ctx = $ctx ?? \Auth::profile();
			$key = $ctx->id;

			if (isset(self::$instances[$key])) {
				return self::$instances[$key];
			}

			$card = UCard::instantiateContexted($ctx);
			self::$instances[$key] = $card;
			self::$active = $card;

			return self::$active;
		}

		public function flagReload()
		{
			self::$_reloadFlag = true;
		}

		public function getUser()
		{
			return $this->card->user;
		}

		public function getImpersonableDomains()
		{
			return $this->card->impersonableDomains;
		}

		public function getName()
		{
			if (!$this->card->name) {
				if ($this->hasPrivilege('site', 'user')) {
					$pwd = $this->user_getpwnam();
					$name = $pwd['gecos'];
				} else {
					if ($this->hasPrivilege('admin')) {
						$name = 'App Admin';
					} else {
						$name = 'Reseller';
					}
				}
				$this->card->name = $name;
			}

			return $this->card->name;
		}

		public function hasPrivilege(...$role)
		{
			if (!$this->card) {
				return false;
			}
			for ($i = 0, $n = sizeof($role); $i < $n; $i++) {
				$p = $role[$i];
				if (!isset(self::$role_map[$p])) {
					return error("unknown role `$p'");
				}
				if ($this->card->level & self::$role_map[$p]) {
					return true;
				}
			}

			return false;
		}

		public function getAvatar()
		{
			return $this->card->avatar;
		}

		public function getDomain()
		{
			return $this->card->domain;
		}

		/**
		 * get account storage usage
		 *
		 */
		public function getStorage()
		{
			$key = $this->card->user . '@' . $this->card->domain . ':dq';
			$quota = apcu_fetch($key, $success);
			if (false && $success) {
				return $quota;
			}
			if ($this->hasPrivilege('site')) {
				$storage = $this->site_get_account_quota();
			} else if ($this->hasPrivilege('user')) {
				$storage = $this->user_get_quota();
			} else if ($this->hasPrivilege('admin')) {
				$storage = $this->admin_get_storage();
			} else {
				report('??? - ' . var_export($this->card, true));
			}
			$quota = array(
				'used'  => $storage['qused'],
				'total' => $storage['qhard'],
				'free'  => MAX(0, $storage['qhard'] - $storage['qused'])
			);
			if ($quota['total'] === 0) {
				$quota['total'] = .0000000000001;
			}
			apcu_store($key, $quota, 1800);

			return $quota;
		}

		/**
		 * get account bandwidth usage
		 *
		 */
		public function getBandwidth()
		{
			$bw = array('free' => 0, 'total' => 0.000000001, 'used' => 0, 'rollover' => 'Unknown');
			if (!$this->hasPrivilege('site')) {
				return $bw;
			}

			$key = $this->card->user . '@' . $this->card->domain . ':bq';
			$quota = apcu_fetch($key, $success);
			if ($success) {
				return $quota;
			}

			$bw = $this->bandwidth_usage();
			$bw['rollover'] = date('F j', $this->bandwidth_rollover());
			if (!isset($bw['total'])) {
				// @XXX debugging
				Error_Reporter::report(var_export($this->getApnscpFunctionInterceptor(), true));
				$bw['total'] = null;
				$bw['used'] = 0;
				$bw['free'] = null;
			} else {
				$bw['total'] /= pow(1024, 3);
				$bw['used'] /= pow(1024, 3);

				$bw['free'] = max(0, $bw['total'] - $bw['used']);
				$bw['total'] += .00000000001;

			}

			apcu_store($key, $bw, 3600);

			return $bw;
		}

		public function getCgroups()
		{
			if (isset($this->_cgroups)) {
				return $this->_cgroups;
			}
			$groups = array();
			$gnames = $this->cgroup_get_controllers();
			foreach ($gnames as $controller) {
				if ([] !== ($usage = $this->cgroup_get_usage($controller))) {
					$groups[$controller] = $usage;
				}
			}
			$this->_cgroups = $groups;

			return $groups;
		}

		/**
		 * Get last timestamp of access
		 *
		 */
		public function lastAccess()
		{
			if (!is_object($this->getApnscpFunctionInterceptor())) {
				$bt = Error_Reporter::get_debug_bt();
				Error_Reporter::report($bt);

				return null;
			}
			$last = $this->auth_get_last_login();
			if (!$last) {
				return null;
			}

			return $last['ts'];

		}

		/**
		 * Get time formatter
		 *
		 * @return string
		 */
		public function getDateTimeLocale(): string
		{
			return 'Y-m-d g:i A';
		}

		public function formatTime(DateTime $d, int $formatType = IntlDateFormatter::FULL): string {
			return $this->format($d, '', IntlDateFormatter::NONE, $formatType);
		}

		public function formatDate(DateTime $d, int $formatType = IntlDateFormatter::FULL): string {
			return $this->format($d, '', $formatType, IntlDateFormatter::NONE);
		}

		public function format(DateTime $d, string $formatMask = '', int $formatDateType = IntlDateFormatter::FULL, int $formatTimeType = IntlDateFormatter::FULL): string
		{
			if (!extension_loaded('intl')) {
				return $d->format('r');
			}

			return (new IntlDateFormatter(
				\Preferences::get($this->getLanguageKey(), 'en-US'),
				$formatDateType,
				$formatTimeType,
				\Preferences::get($this->getTimezoneKey(), date_default_timezone_get()),
				IntlDateFormatter::GREGORIAN,
				$formatMask
			))->format($d);
		}

		/**
		 * Flush UCard cache
		 *
		 */
		public function flush()
		{
			$key = $this->card->user . '@' . $this->card->domain;
			apcu_delete($key . ':dq');
			apcu_delete($key . ':bq');
		}

		public function getRoleAsString()
		{
			$keys = array_keys(self::$role_map);
			for ($i = 0, $n = sizeof($keys); $i < $n; $i++) {
				$role = $keys[$i];
				if ($this->hasPrivilege($role)) {
					return $role;
				}
			}
		}

		public function setPref($name, $val = null)
		{
			return \Preferences::set($name, $val);
		}

		public function setPref2($name, $key, $val = null)
		{
			return \Preferences::set($name . '.' . $key, $val);
		}

		public function getPref($name)
		{
			return \Preferences::get($name);
		}

		public function getPref2($name, $key)
		{
			return \Preferences::get($name . '.' . $key);
		}

		public function getGoogleApiKey()
		{
			return 'google-api-client-id';
		}

		public function getTimezoneKey()
		{
			return 'timezone';
		}

		public function getLanguageKey()
		{
			return 'language';
		}

		protected function email()
		{
			if ($this->level & PRIVILEGE_SITE) {

			} else {
				if ($this->level & PRIVILEGE_ADMIN) {

				} else {
					return null;
				}
			}
		}

	}