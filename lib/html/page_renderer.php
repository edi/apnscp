<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Page_Renderer
	{
		const THEME_KEY = 'theme.name';
		const THEME_SWAP_BUTTONS = 'theme.swap-buttons';
		const THEME_MENU_ALWAYS_COLLAPSE = 'theme.collapse-menu';
		const THEME_EXTERNAL_OPENER = 'theme.external-opener';

		const DISP_NO_BREADCRUMBS = 0x0001;
		const DISP_NO_OVERVIEW = 0x0002;
		const DISP_NO_HELP = 0x0004;
		const DISP_NO_HEADER = 0x0008;
		const DISP_NO_FOOTER = 0x0400;
		const DISP_FULL_HELP = 0x0010;
		const DISP_NO_NAV = 0x0020;
		const DISP_NO_NOTIFIER = 0x0040;
		const DISP_ON_ERROR = 0x0080;
		const HIDE_BUILTIN_RENDERER = 0x0100;
		const DISP_NOTIFIER = 0x0200;
		const DISP_TUTORIAL = 0x0400;

		private static $options = 0x0080;

		public static function hide_all()
		{
			self::$options |= 0xFFFF & ~self::DISP_FULL_HELP & ~self::HIDE_BUILTIN_RENDERER;
		}

		public static function display_postback_notifier()
		{
			self::$options = (self::$options|self::DISP_NOTIFIER) & ~self::DISP_NO_NOTIFIER;
		}

		public static function hide_postback_notifier()
		{
			self::$options |= self::DISP_NO_NOTIFIER;
		}

		public static function display_on_error()
		{
			self::$options |= self::DISP_ON_ERROR;
		}

		public static function hide_on_error()
		{
			self::$options |= ~self::DISP_ON_ERROR;
		}

		public static function hide_breadcrumbs()
		{
			self::$options |= self::DISP_NO_BREADCRUMBS;
		}

		public static function hide_nav()
		{
			self::$options |= (self::DISP_NO_NAV | self::DISP_NO_BREADCRUMBS);
		}

		public static function hide_header()
		{
			self::$options |= self::DISP_NO_HEADER;
		}

		public static function hide_footer()
		{
			self::$options |= self::DISP_NO_FOOTER;
		}

		public static function hide_overview()
		{
			self::$options |= self::DISP_NO_OVERVIEW | self::DISP_NO_HELP;
		}

		public static function hide_tutorial()
		{
			self::$options &= ~self::DISP_TUTORIAL;
		}

		public static function hide_full_help()
		{
			self::$options |= ~self::DISP_FULL_HELP;
		}

		public static function show_full_help()
		{
			self::$options |= self::DISP_FULL_HELP;
		}

		public static function show_tutorial()
		{
			self::$options |= self::DISP_TUTORIAL;
		}

		public static function do_overview()
		{
			return ~self::$options & self::DISP_NO_OVERVIEW;
		}

		public static function do_display()
		{
			return self::$options & self::DISP_ON_ERROR;
		}

		public static function do_help()
		{
			return ~self::$options & self::DISP_NO_HELP;
		}

		public static function do_notifier()
		{
			return ~self::$options & self::DISP_NO_NOTIFIER;
		}

		public static function do_full_help()
		{
			return self::$options & self::DISP_FULL_HELP;
		}

		public static function do_breadcrumbs()
		{
			return ~self::$options & self::DISP_NO_BREADCRUMBS;
		}

		public static function do_nav()
		{
			return ~self::$options & self::DISP_NO_NAV;
		}

		public static function do_footer()
		{
			return ~self::$options & self::DISP_NO_FOOTER;
		}

		public static function do_header()
		{
			return ~self::$options & self::DISP_NO_HEADER;
		}

		public static function do_tutorial()
		{
			return self::$options & self::DISP_TUTORIAL;
		}

		public static function set($option)
		{
			self::$options |= $option;
		}

		public static function get_options()
		{
			return self::$options;
		}

		/**
		 * Always use external opener
		 *
		 * @return bool
		 */
		public static function externalOpener(): bool
		{
			return \Preferences::get(self::THEME_EXTERNAL_OPENER, FRONTEND_EXTERNAL_OPENER);
		}
	}