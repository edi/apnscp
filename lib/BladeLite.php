<?php

	use Illuminate\Support\Facades\Facade;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */
	class BladeLite extends Jenssegers\Blade\Blade implements \Illuminate\Contracts\View\Factory
	{
		const CACHE_PATH = 'framework/cache';
		protected static $instance;

		/**
		 * Instantiate a new Blade instance
		 *
		 * @param array|string $viewPaths
		 * @return \Jenssegers\Blade\Blade
		 */
		public static function factory($viewPaths = 'views'): self
		{
			if (is_string($viewPaths) && $viewPaths[0] !== '/') {
				$viewPaths = str_replace('.', '/', $viewPaths);
				$paths = [resource_path('')];
				if (class_exists(\Lararia\Application::class, false)) {
					// if \Lararia\Bootstrapper::minstrap() called, use full config
					$paths = array_get(\Lararia\Application::getInstance()['config'], 'view.paths', []);
				} else if (file_exists($file = conf_path('laravel/view.php'))) {
					$paths = array_get((array)include($file), 'paths', $paths);
				}
				foreach ($paths as $key => $path) {
					// append path to lookups, may change in future
					if ($path && file_exists($tmp = $path . DIRECTORY_SEPARATOR . $viewPaths)) {
						$paths[$key] = $tmp;
						continue;
					}
					unset($paths[$key]);
				}
				$viewPaths = $paths;
			}
			$c = new static($viewPaths, storage_path(self::CACHE_PATH));

			$c->container->get('view')->addNamespace('theme', [
				config_path('custom/resources/views'),
				resource_path('views')
			]);
			$c->container->get('view')->addNamespace('master', [
				config_path('custom/resources/views'),
				resource_path('views')
			]);

			foreach (\Module\Support\Webapps\PathManager::applicationViewPaths() as $appName => $paths) {
				$c->container->get('view')->addNamespace("@webapp(${appName})", $paths);
			}

			$c->component('theme::partials.app.modal','modal');

			$c->compiler()->directive('lang', static function ($expression) use ($c) {
				return "<?php echo ArgumentFormatter::format($expression) ?>";
			});

			$c->compiler()->directive('inline', static function ($expression) use ($c) {
				if ($expression[0] == '(') {
					// weird, on helios the directive comes through as "(email.css)"
					$expression = trim($expression, '()');
				}
				if ($expression[0] != '/') {
					$paths = $c->container->config['view.paths'];
					foreach ($paths as $p) {
						if (file_exists("${p}/${expression}")) {
							$expression = "${p}/${expression}";
							break;
						}
					}
				}

				return file_get_contents($expression);
			});

			return $c;
		}

		public function component($path, $alias = null)
		{
			$alias = $alias ?: array_last(explode('.', $path));

			$this->compiler()->directive($alias, static function ($expression) use ($path) {
				return $expression
					? "<?php \$__env->startComponent('{$path}', {$expression}); ?>"
					: "<?php \$__env->startComponent('{$path}'); ?>";
			});

			$this->compiler()->directive('end' . $alias, static function ($expression) {
				return '<?php echo $__env->renderComponent(); ?>';
			});
		}

		/**
		 * Create or get a Blade instance and update paths as necessary
		 *
		 * @param string $viewPaths
		 * @return Jenssegers\Blade\Blade
		 */
		public static function singleton($viewPaths = null)
		{
			if (null !== self::$instance && $viewPaths) {
				self::$instance->updateConfig($viewPaths);
			} else if (null === self::$instance) {
				self::$instance = new static($viewPaths, storage_path(self::CACHE_PATH));
			}

			return self::$instance;
		}

		public function emptyPaths()
		{
			$this->viewPaths = [];
			$this->setConfig();
		}

		protected function setConfig()
		{
			$this->container->bind('config', function () {
				return [
					'view.paths'    => (array)$this->viewPaths,
					'view.compiled' => $this->cachePath,
				];
			}, true);
		}

		public function updateConfig($paths)
		{
			$config = array_get($this->container->config, 'view.paths', []);
			$config = array_merge($config, (array)$paths);
			$this->viewPaths = $config;
			$this->setConfig();
		}

		public function exists($view): bool
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		public function file($path, $data = [], $mergeData = []): \Illuminate\Contracts\View\View
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		public function make($view, $data = [], $mergeData = []): \Illuminate\Contracts\View\View
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		public function composer($views, $callback): array
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		public function creator($views, $callback): array
		{
			return $this->__call(__FUNCTION__, func_get_args());
		}

		protected function setupContainer(array $viewPaths, string $cachePath)
		{
			$accessor = Facade::getFacadeApplication();
			parent::setupContainer($viewPaths, $cachePath);
			if (null !== $accessor) {
				Facade::setFacadeApplication($accessor);
			}
		}


	}