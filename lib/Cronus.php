<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */


class Cronus {
	const CALLBACK_ORDER = [
		'cb',
		'last'
	];
	/**
	 * @var \Cronus\Timekeeper
	 */
	protected $time;

	private $cb = [
		'cb' => [],
		'last' => []
	];

	/**
	 * Register unordered callback
	 *
	 * @param Closure $fn
	 */
	public function register(Closure $fn) {
		$this->cb['cb'][] = $fn;
	}

	/**
	 * Register callback at end of cron
	 *
	 * @param Closure $fn
	 */
	public function end(Closure $fn) {
		$this->cb['last'][] = $fn;
	}

	public function __destruct()
	{
		$this->time->update();

		foreach (static::CALLBACK_ORDER as $order) {
			foreach ($this->cb[$order] as $cb) {
				$cb();
			}
		}
		// deinit, primarily for PostgreSQL so any checkpoints can flush
		\MySQL::initialize()->close();
		\PostgreSQL::initialize()->close();
	}

	public function __construct(\Cronus\Timekeeper $time = null)
	{
		// reinit
		\MySQL::initialize();
		\PostgreSQL::initialize();
		$this->time = $time ?? new \Cronus\Timekeeper;
	}

	/**
	 * Schedule task identified by name
	 *
	 * @param int     $interval interval to run task
	 * @param string  $name     symbolic name
	 * @param Closure $closure  callback
	 */
	public function schedule(int $interval, string $name, Closure $closure): void
	{
		$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
		$callee = $backtrace[1]['class'] ?? null;
		if (!$callee || $backtrace[1]['function'] !== '_cron') {
			fatal('scheduled() called outside of _cron ');
		}
		$module = apnscpFunctionInterceptor::get_module_from_class($backtrace[1]['class']);
		$hash = "$module:$name";
		$now = time();
		$countdown = \Cache_Global::spawn()->hIncrBy('cron.tasks', $hash, -($now - $this->time->get()));
		if (0 >= $countdown) {
			debug('Timer elapsed. %d seconds, calling %s', $interval, $module);
			try {
				$closure();
			} finally {
				$cache = \Cache_Global::spawn();
				$oldSerializer = $cache->getOption(Redis::OPT_SERIALIZER);
				$cache->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
			
				$cache->hSet('cron.tasks', $hash, $interval);
				$cache->setOption(Redis::OPT_SERIALIZER, $oldSerializer);
			{
		} else {
			debug('Timer elapsed. %d seconds, %d remain. No action on %s',
				$now - $this->time->get(),
				$countdown,
				$hash
			);
		}
	}
}
