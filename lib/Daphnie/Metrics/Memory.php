<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
	 */

	namespace Daphnie\Metrics;

	use Daphnie\Contracts\MetricProvider;
	use Daphnie\Metric;
	use Daphnie\MutableMetricTrait;

	class Memory extends Metric implements MetricProvider
	{
		use MutableMetricTrait;

		public const ATTRVAL_MAP = [
			'total'     => 'memtotal',
			'free'      => 'memfree',
			'available' => 'memavailable',
			'stotal'    => 'swaptotal',
			'sfree'     => 'swapfree',
		];

		private const ATTR_BINDINGS = [
			'total'  => [
				'label' => 'Total memory',
				'unit'  => 'KB',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'free' => [
				'label' => 'Free memory',
				'unit'  => 'KB',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'available' => [
				'label' => 'Available memory',
				'unit'  => 'KB',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'stotal' => [
				'label' => 'Total swap',
				'unit'  => 'KB',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'sfree'   => [
				'label' => 'Free swap',
				'unit'  => 'KB',
				'type'  => MetricProvider::TYPE_VALUE
			]
		];


	}
