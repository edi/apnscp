<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Skeleton;

	use Module\Definition\MODULE30;
	use Module\Skeleton\Contracts\Proxied;

	/**
	 * Class Module_Skeleton
	 */
	abstract class Standard implements MODULE30
	{
		use \apnscpFunctionInterceptorTrait;
		use \FilesystemPathTrait;
		use \AccountInfoTrait;

		const DEPENDENCY_MAP = ['siteinfo'];

		protected $exportedFunctions = array();

		protected $username;
		protected $password;
		protected $domain;
		protected $user_id;
		protected $group_id;
		protected $permission_level = 0;
		protected $session_id = null;
		protected $site;
		protected $site_id;

		public function __construct()
		{
			$this->initializeUser();
		}

		private function initializeUser()
		{
			if ($this->inContext()) {
				return $this->setUserParameters($this->getAuthContext());
			}
			if (!IS_CLI && $_SESSION) {
				if (!isset($_SESSION['username'])) {
					\apnscpSession::init()->destroy(\session_id());
					fatal('Session corruption - %s', \session_id());
				}
				$this->group_id = (int)$_SESSION['group_id'];
				$this->user_id = (int)$_SESSION['user_id'];
				$this->username = $_SESSION['username'];
				$this->password = $_SESSION['password'] ?? null;
				$this->session_id = session_id();
				$this->domain = $_SESSION['domain'];
				$this->permission_level = $_SESSION['level'];
				/** we'll need to fix up handling this information */
				$this->site_id = $_SESSION['site_id'];
				$this->site = 'site' . $this->site_id;
			}
		}

		public function setUserParameters(\Auth_Info_User $auth)
		{
			if ($auth->id === $this->session_id) {
				return true;
			}

			if (!$auth->id) {
				fatal('no session id');
			}

			$this->domain = $auth->domain;
			$this->site_id = $auth->site_id;
			$this->site = $auth->site;
			$this->username = $auth->username;
			$this->permission_level = $auth->level;
			$this->group_id = $auth->group_id;
			$this->user_id = $auth->user_id;
			$this->session_id = $auth->id;
			$this->setContext($auth);
		}

		/**
		 * Autoload instance using context
		 *
		 * @param \Auth_Info_User $context
		 * @return Standard
		 */
		public static function autoloadModule(\Auth_Info_User $context): \Module_Skeleton
		{
			$c = static::instantiateContexted($context);
			if (\in_array(Proxied::class, class_implements($c), true)) {
				return $c->_proxy();
			}

			return $c;
		}

		public function __wakeup()
		{
			// refresh account meta if necessary
			// failure to do so in contexted instances will yield stale metadata
			$id = null;
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
			try {
				$id = \Auth::profile()->id;
			} catch (\apnscpException $e) {
				// pass
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}

			if ($id === $this->session_id) {
				if (\session_id() !== $this->session_id && !\apnscpSession::restore_from_id($this->session_id)) {
					fatal('session logic mismatch: wanted %s got %s', \session_id(), $this->session_id);
				}
				$this->setContext(\Auth::profile());
			} else if ($this->inContext()) {
				$this->getAuthContext()->reset();
				$this->setContext($this->getAuthContext());
			}
			$this->initializeUser();
		}

		/**
		 * Call method on module
		 *
		 * @param string $function
		 * @param mixed  $args
		 * @return mixed
		 */
		public function _invoke($function, $args)
		{
			return $this->$function(...$args);
		}

		public function __debugInfo()
		{
			return [
				'session_id' => $this->session_id,
				'username'   => $this->username,
				'site'       => $this->site
			];
		}

		public function getExportedFunctions(): array
		{
			return $this->exportedFunctions;
		}

		/**
		 * Account reset hook
		 *
		 * @param \Util_Account_Editor|null $editor
		 * @return array
		 */
		public function _reset(\Util_Account_Editor &$editor = null)
		{
			return array();
		}

		/** {{{ void clean_user_parameters
		 * Cleans the user-specific parameters; necessary to invoke after calling
		 * a backend call
		 */
		public function cleanUserParameters()
		{
			unset(
				$this->username, $this->domain,
				$this->password, $this->session_id, $this->user_id,
				$this->group_id, $this->permission_level, $this->authContext
			);

			return $this;
		}

		/**
		 * Elevate permissions + query backend
		 *
		 * @param string $cmd
		 * @param mixed  $args
		 * @return mixed
		 */
		public function query($cmd, ...$args)
		{
			if (IS_CLI) {
				return $this->__call($cmd, $args);
			}
			$ret = \DataStream::get($this->getAuthContext())->query($cmd, ...$args);

			return $ret;
		}
	}
