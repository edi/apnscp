<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Util_API extends SoapClient
	{
		use apnscpFunctionInterceptorTrait {
			__call as callProtected;
		}

		const DEFAULT_SERVER = 'localhost';
		const PORT = 2082;
		const SECURE_PORT = 2083;
		const WSDL_PATH = SOAP_WSDL;

		/**
		 * Create new API client
		 *
		 * @param       $key
		 * @param null  $server
		 * @param null  $port
		 * @param array $ctor additional constructor arguments to SoapClient
		 * @return self
		 */
		public static function create_client($key, $server = null, $port = null, array $ctor = []): self
		{
			if (!$server) {
				$server = self::DEFAULT_SERVER . ':' . self::PORT;
			} else {
				if (!$port) {
					$port = self::PORT;
				}
				$server .= ':' . $port;
			}
			$proto = $port === self::PORT || !$port ? 'http' : 'https';
			$uri = $proto . '://' . $server . '/soap';
			$wsdl = str_replace('/soap', '/' . self::WSDL_PATH, $uri);

			$connopts = $ctor + array(
				'connection_timeout' => 30,
				'location'           => $uri,
				'uri'                => 'urn:apnscp.api.soap',
				'trace'              => is_debug(),
			);
			$connopts['location'] = $uri . '?authkey=' . $key;

			return new static($wsdl, $connopts);
		}

		public static function create_key($comment = null)
		{
			$afi = apnscpFunctionInterceptor::init();
			$key = $afi->auth_create_api_key($comment);

			return $key;
		}

		public function __call($function_name, $arguments)
		{
			static $ctr = 0;
			$ret = @parent::__call($function_name, $arguments);
			if ($ret !== null || $ctr >= 5) {
				$ctr = 0;

				return $ret;
			}
			// 50 ms sleep
			usleep(50000);
			$ctr++;

			return $this->__call($function_name, $arguments);
		}

		public static function get_key($search = '')
		{
			$afi = apnscpFunctionInterceptor::init();
			$keys = $afi->auth_get_api_keys();
			foreach ($keys as $k) {
				if (false !== strpos($k['comment'], $search)) {
					return $k;
				}
			}

			return null;
		}
	}
