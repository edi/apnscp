<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

namespace Service\CaptureDevices\Contracts;

use Util\Process\Contracts\StandaloneService;

interface CaptureService extends StandaloneService {
	/**
	 * Fake DNS for host
	 *
	 * @param string $host
	 * @param string $addr
	 */
	public function addHost(string $host, string $addr);

	/**
	 * Wait for chromedriver connection
	 *
	 * @return bool
	 */
	public function wait(): bool;
}