<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	/**
	 * Session-less fixation of secondary accounts
	 *
	 * Mix with apnscpFunctionInterceptor trait for afi invocation
	 * Mix with \Preferences::factory() for pref manipulation
	 */
	trait ContextableTrait
	{
		/**
		 * @var \Auth_Info_User
		 */
		private $authContext;

		/**
		 * Substitute apnscpFunctionInterceptor then build class
		 *
		 * @param Auth_Info_User $context
		 * @param array          $constructorArgs arguments to provide to constructor
		 * @return self
		 */
		public static function instantiateContexted(\Auth_Info_User $context, array $constructorArgs = []): self
		{
			$rfxn = (new \ReflectionClass(static::class))->newInstanceWithoutConstructor();
			$rfxn->setContext($context);
			if (method_exists($rfxn, '__construct')) {
				$rfxn->__construct(...$constructorArgs);
			}

			return $rfxn;
		}

		/**
		 * Set context for afi or Preference invocation
		 *
		 * @param Auth_Info_User $context
		 * @return self
		 */
		public function setContext(\Auth_Info_User $context): self
		{
			$this->authContext = $context;
			if (method_exists($this, 'setApnscpFunctionInterceptor')) {
				$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($context));
			}
			if (method_exists($this, 'bindPathContext')) {
				$this->bindPathContext($context);
			}
			return $this;
		}

		/**
		 * Module calls in authenticiation context
		 *
		 * @return bool
		 */
		protected function inContext(): bool
		{
			return null !== $this->authContext;
		}

		/**
		 * Get user authentication instance
		 *
		 * @return \Auth_Info_User
		 */
		protected function getAuthContext(): \Auth_Info_User
		{
			return $this->authContext ?? \Auth::autoload()->getProfile();
		}

		/**
		 * Force refresh of authContext. Useful when a backend update occurs
		 * and results are not immediately obvious during a frontend lifecycle
		 */
		protected function freshenAuthContext(): void
		{
			if (!method_exists($this, 'getApnscpFunctionInterceptor')) {
				fatal('%(self)s may only be called when stacked with %(trait)s',
					['self' => __FUNCTION__, 'trait' => apnscpFunctionInterceptorTrait::class]
				);
			}

			if (!\apnscpSession::init()->exists($this->getAuthContext()->id)) {
				$this->authContext = \Auth::context($this->getAuthContext()->username, $this->getAuthContext()->site);
			} else {
				$this->getAuthContext()->reset();
			}

			if (!$this->getApnscpFunctionInterceptor()->context_matches_self($this->getAuthContext())) {
				$this->getApnscpFunctionInterceptor()->set_session_context($this->getAuthContext());
			}
		}

		/**
		 * Context matches supplied context
		 *
		 * @param Auth_Info_User $ctx
		 * @return bool
		 */
		public function contextSynchronized(\Auth_Info_User $ctx): bool
		{
			return $ctx === $this->getAuthContext();
		}
	}