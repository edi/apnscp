<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Dav\Backends;

	use Sabre\DAV as Provider;

	class GlobalPropertyStorage extends Provider\PropertyStorage\Backend\PDO
	{
		// clean up apnscp db
		const SITE_SEPARATOR = '.';
		public $tableName = 'dav_propertystorage';

		/**
		 * Fetches properties for a path.
		 *
		 * This method received a PropFind object, which contains all the
		 * information about the properties that need to be fetched.
		 *
		 * Usually you would just want to call 'get404Properties' on this object,
		 * as this will give you the _exact_ list of properties that need to be
		 * fetched, and haven't yet.
		 *
		 * However, you can also support the 'allprops' property here. In that
		 * case, you should check for $propFind->isAllProps().
		 *
		 * @param string            $path
		 * @param Provider\PropFind $propFind
		 * @return void
		 */
		public function propFind($path, Provider\PropFind $propFind)
		{
			return parent::propFind(
				$this->normalize($path),
				$propFind
			);
		}

		protected function normalize($path)
		{
			if (!empty($_SESSION['level']) & (PRIVILEGE_USER | PRIVILEGE_SITE)) {
				return 'site' . $_SESSION['site_id'] . self::SITE_SEPARATOR . $path;
			}

			return $path;
		}

		/**
		 * Updates properties for a path
		 *
		 * This method received a PropPatch object, which contains all the
		 * information about the update.
		 *
		 * Usually you would want to call 'handleRemaining' on this object, to get;
		 * a list of all properties that need to be stored.
		 *
		 * @param string             $path
		 * @param Provider\PropPatch $propPatch
		 * @return void
		 */
		public function propPatch($path, Provider\PropPatch $propPatch)
		{
			return parent::propPatch(
				$this->normalize($path),
				$propPatch
			);
		}

		/**
		 * This method is called after a node is deleted.
		 *
		 * This allows a backend to clean up all associated properties.
		 *
		 * The delete method will get called once for the deletion of an entire
		 * tree.
		 *
		 * @param string $path
		 * @return void
		 */
		public function delete($path)
		{
			return parent::delete(
				$this->normalize($path)
			);
		}

		/**
		 * This method is called after a successful MOVE
		 *
		 * This should be used to migrate all properties from one path to another.
		 * Note that entire collections may be moved, so ensure that all properties
		 * for children are also moved along.
		 *
		 * @param string $source
		 * @param string $destination
		 * @return void
		 */
		public function move($source, $destination)
		{
			return parent::move(
				$this->normalize($source),
				$this->normalize($destination)
			);
		}
	}