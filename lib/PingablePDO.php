<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2021
 */

class PingablePDO extends  PDO
{
	private array $params;

	public function __construct($dsn, $username = null, $password = null, $options = null)
	{
		$this->params = func_get_args();
		parent::__construct(...$this->params);
	}

	private function init()
	{
		try {
			return new static(...$this->params);
		} catch (\PDOException $e) {
			if ($e->errorInfo[0] === "08006" /* database system is in recovery mode */) {
				sleep(1);
				return $this->init();
			}
			fatal($e->getMessage());
		}
		return $this;
	}

	public function ping(): PDO
	{
		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
		try {
			$this->exec("SELECT 1");
		} catch (\PDOException $e) {
			if ($e->errorInfo[0] === 'HY000') {
				// disconnected
				return $this->init();
			}
			throw $e;
		} finally {
			\Error_Reporter::exception_upgrade($oldex);
		}

		return $this;
	}
}