<?php

	class HTTP_Request2_Observer_SaveDisk implements SplObserver
	{
		protected $dir;
		protected $filename;

		protected $fp;

		public function __construct($file)
		{
			if (is_dir($file)) {
				$this->dir = $file;
			} else {
				$dir = dirname($file);
				$file = basename($file);
				if (!is_dir($dir)) {
					throw new Exception($dir . ': is not a directory');
				}
				$this->dir = $dir;
				$this->filename = $file;
			}
		}

		public function __destruct()
		{
			if (is_resource($this->fp)) {
				fclose($this->fp);
			}
		}

		public function update(SplSubject $subject)
		{
			$event = $subject->getLastEvent();

			switch ($event['name']) {
				case 'receivedHeaders':
					if (!$this->filename) {
						if (($disposition = $event['data']->getHeader('content-disposition'))
							&& 0 == strpos($disposition, 'attachment')
							&& preg_match('/filename="([^"]+)"/', $disposition, $m)
						) {
							$filename = basename($m[1]);
						} else {
							$filename = basename($subject->getUrl()->getPath());
						}
					}

					$target = $this->dir . DIRECTORY_SEPARATOR . $this->filename;
					if (!($this->fp = fopen($target, 'wb'))) {
						throw new Exception("Cannot open target file '{$target}'");
					}
					break;

				case 'receivedBodyPart':
				case 'receivedEncodedBodyPart':
					fwrite($this->fp, $event['data']);
					break;

				case 'receivedBody':
					fclose($this->fp);
			}
		}
	}

?>