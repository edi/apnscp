<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Event;

	use Event\Contracts\Publisher;
	use Event\Contracts\Subscriber;

	/**
	 * Class Cardinal
	 *
	 * Event-driven callbacks that register without inversion
	 *
	 * @package Event
	 */
	class Cardinal
	{
		// failure of event terminates entire chain
		const OPT_ATOMIC = 0x01;
		// event inserted at head
		const OPT_PREPEND = 0x02;
		// event cancels all existing events in space
		const OPT_SINGLE = 0x04;

		private static $instance;

		/**
		 * Delete all triggers
		 */
		public static function purge(): void
		{
			self::$instance = null;
		}

		/**
		 * Remove all named triggers from manager
		 *
		 * @param array|string $event
		 */
		public static function deregisterAll($event): void
		{
			$instance = self::getInstance();
			$instance->remove(implode('.', (array)$event), null);
		}

		/**
		 * Create a global event handler
		 *
		 * @return Manager
		 */
		protected static function getInstance(): Manager
		{
			if (null === self::$instance) {
				self::$instance = new Manager();
			}

			return self::$instance;
		}

		/**
		 * Remove a specific event handler
		 *
		 * @param array|string $event
		 * @param Subscriber   $instance
		 * @return void
		 */
		public static function deregister($event, Subscriber $instance): void
		{
			$queue = self::getInstance();
			$queue->remove(implode('.', (array)$event), $instance);
		}

		/**
		 * Push event to front of queue
		 *
		 * @param $event
		 * @param $instance
		 * @return Subscriber transformed callback
		 */
		public static function preempt($event, $instance): Subscriber
		{
			return static::register($event, $instance, self::OPT_PREPEND);
		}

		/**
		 * Register a named event in the global event queue
		 *
		 * Events may either be compound objects or single, global events
		 * e.g. create.fail and fail are two separate registered events.
		 * edit.fail and create.fail would both fire a "fail" event
		 * in addition to their respective compound events
		 *
		 * @param array|string event  $event
		 * @param Subscriber|callable $instance
		 * @param int                 $opts options
		 * @return Subscriber transformed callback
		 */
		public static function register($event, $instance, int $opts = 0): Subscriber
		{
			$queue = self::getInstance();
			$cb = self::convertClosure($instance);
			$queue->register($event, $cb, $opts);

			return $cb;
		}

		/**
		 * Convert closure to Subscriber
		 *
		 * @param $cb \Closure|Subscriber
		 * @return Subscriber
		 */
		protected static function convertClosure($cb): Subscriber
		{
			if ($cb instanceof Subscriber) {
				return $cb;
			}
			if (!\is_callable($cb)) {
				fatal("unknown callback registered `%s'", \get_class($cb));
			}

			return new class($cb) implements Subscriber
			{
				protected $cb;

				public function __construct($t)
				{
					$this->cb = $t;
				}

				public function update($event, Publisher $caller)
				{
					return \call_user_func($this->cb, $event, $caller);
				}
			};
		}

		/**
		 * Register a named event; false terminates all registered events in the namespace
		 *
		 * @param array|string        event $event
		 * @param Subscriber|callable $instance
		 * @return Subscriber transformed callback
		 */
		public static function atomic($event, $instance): Subscriber
		{
			return static::register($event, $instance, self::OPT_ATOMIC);
		}

		/**
		 * Fire a registered event
		 *
		 * @param array|string $event event, supports dot notation
		 * @param Publisher    $caller
		 * @return bool|null callback status or null if no callbacks fired
		 *
		 */
		public static function fire($event, Publisher $caller): ?bool
		{
			$queue = self::getInstance();

			return $queue->fire($event, $caller);
		}

		/**
		 * Event matches pattern
		 *
		 * @param string|array $event
		 * @param string       $pattern
		 * @return bool
		 */
		public static function is($event, string $pattern): bool
		{
			if (\is_array($event)) {
				$event = implode('.', $event);
			}

			return substr($event, -\strlen($pattern)) === $pattern;
		}
	}
