<div class="alert alert-{{ $status ?? 'success' }}">
    {{ $slot }}
</div>