@if ($state == 'close')
    The support ticket has been closed.
@endif
============================
Ticket Info
============================

URL: {!! $url !!}
Reference ID: {{ $ticket_id }}
@if ($recipient == 'admin')
Domain: {{ $domain }}
Category: {{ $category }}
Priority: {{ $priority }}
@endif
Email: {{ $email }}

=============================

{{ $data }}

=============================

@if ($state == 'close')
This ticket may be reopened during the next 4 weeks.
Access the ticket from the control panel, then change the state to "Reopen" to add additional notes.
@endif

Thank you for using {{ PANEL_BRAND }}!
Support Staff <{{ \Crm_Module::FROM_ADDRESS }}>
