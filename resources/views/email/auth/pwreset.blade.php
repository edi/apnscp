@extends("email.auth.auth-common")
@section('title', "Password Reset Request")
@section("notice")
    Hello!<br/>
    <p>
        Here is your requested information:
        <br/><br/>
        ******<br/>
        <b>USER:</b> <code>{{ $username }}</code><br/>
        <b>DOMAIN:</b> <code>{{ $domain }}</code><br/>
        ******
        <br/><br/>
    </p>

<a href="{{ $url }}" class="btn-primary">Reset Password</a>

<h3>Manual reset instructions</h3>
Access the control panel via <a href="{{ $manualurl }}">{{ $manualurl }}</a> and enter the challenge token: <br/><br/>
<code style="margin-left: 40px;">{{ $token }}</code>
    <br />
@endsection
