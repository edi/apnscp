@component('mail::message')
Hello,

One or more sites have exceeded their bandwidth usage for the current cycle period on
{{ SERVER_NAME }} (`{{ \Opcenter\Net\Ip4::my_ip() }}`). Below summarizes the sites and states.

# Overages
| Domain          | Over  | Threshold | Cycle End |
|:----------------|:------|:----------|:----------|
@foreach ($items as $overage)
@php /** @var \Opcenter\Bandwidth\Overage $overage */ @endphp
| {{ $overage->getHandler()->amnestied() ? '(A) ' : ($overage->triggersSuspension() ? '(S) ' : '') }} {{ \Auth::get_domain_from_site_id($overage->getSiteId()) }} | {{ sprintf("%.2f GB", \Formatter::changeBytes($overage->getOverage(), 'GB', 'B')) }} | {{ sprintf("%.2f GB", \Formatter::changeBytes($overage->getThreshold(), 'GB', 'B')) }} | {{ date('Y-m-d', $overage->getEnd()) }} |
@endforeach

---

## Tips
Amnesty may be applied to any site from the command-line via `cpcmd bandwidth:amnesty siteXX`. Amnesty
will forgive bandwidth overages for the remainder of the cycle period.

Automatic suspension behavior may be adjusted using `cpcmd scope:set cp.config bandwidth stopgap PCT` where *PCT*
is a number between 100 and 300, a whole percentage that will automatically suspend a site if its bandwidth exceeds PCT%
its quota. Set to 0 to disable this emergency stopgap behavior.

Notification thresholds may be tuned using `cpcmd scope:set cp.config bandwidth notify PCT` where *PCT* is a number
between 50 and 200 (and below [bandwidth] => stopgap). Notices will be send to the contact address when a site has consumed
PCT% percentage of its bandwidth for the cycle period. *Setting this to 0 will disable all bandwidth overage services.*

@endcomponent

