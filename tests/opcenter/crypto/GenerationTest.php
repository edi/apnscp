<?php
	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class GenerationTest extends TestFramework
	{
		public function testSelfSignedInstallation()
		{
			$ephemeral = \Opcenter\Account\Ephemeral::create(['mail.enabled' => 0]);
			$this->assertTrue($ephemeral->getApnscpFunctionInterceptor()->ssl_self_sign($ephemeral->getContext()->domain));
			$ssl = $ephemeral->getApnscpFunctionInterceptor()->ssl_get_certificate();
			$this->assertTrue($ephemeral->getApnscpFunctionInterceptor()->ssl_is_self_signed($ssl), 'Certificate is self-signed');
		}
	}