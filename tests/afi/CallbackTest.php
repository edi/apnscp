<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */
require_once dirname(__DIR__, 1) . '/TestFramework.php';

class CallbackTest extends TestFramework
{
	/**
	 * @var apnscpFunctionInterceptor
	 */
	protected $afi;

	public function setUp()
	{
		$this->afi = \apnscpFunctionInterceptor::init(true);
		$this->afi->get_loaded_modules()->cleanDynamicCompositions();
	}

	public function testCallback() {
		$self = null;
		\a23r::registerCallback('common', 'whoami', function ($ret, $args) use(&$self) {
			$self = $ret;
		});
		$this->assertEquals(\Auth::profile()->username, $this->afi->common_whoami());
		$this->assertNotNull($self);
		$this->assertEquals($self, \Auth::profile()->username);
	}

	public function testReplacedCallback() {
		$self = null;
		\a23r::registerCallback('common', 'whoami', function ($ret, $args) use (&$self) {
			$self = $ret;
		});

		$this->assertEquals(\Auth::profile()->username, $this->afi->common_whoami());
		$this->assertNotNull($self);
		$this->assertEquals($self, \Auth::profile()->username);

		$this->afi->get_loaded_modules()->cleanDynamicCompositions();
		\a23r::registerCallback('common', 'whoami', function ($ret, $args = []) use (&$self) {
			$self = $args;
		});

		$this->assertEquals(\Auth::profile()->username, $this->afi->common_whoami());
		$this->assertIsArray($self);
	}

	public function testRuntimeCallback()
	{
		$self = null;
		\a23r::registerCallback('common', 'whoami', function ($ret, $args) use (&$self) {
			$self = $ret;
		});

		$ret = $this->afi->common_whoami();
		$this->assertNotNull($self);

		\a23r::registerCallback('common', 'get_email', function ($ret, $args) use (&$self) {
			$self = 'abc';
		});

		$this->afi->common_get_email();
		$this->assertEquals($ret, $self);

		$this->afi->get_loaded_modules()->cleanDynamicCompositions();

		$this->afi->common_get_email();
		$this->assertEquals('abc', $self);

	}
}

