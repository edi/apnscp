@include('partials.about', ['service' => 'Mail'])

<div class="d-flex align-items-center align-content-around mb-2">
	<h4 class="mb-0 mr-2">
		{{ _("Incoming mail") }}
	</h4>
	<a href="{{ MISC_KB_BASE }}{{ $vars['kb']['mail-type'] }}" class="ml-auto ui-action ui-action-label ui-action-kb"
	>Help me decide</a>
</div>
<table class="table table-striped table-responsive">
	<thead>
	<tr>
		<th></th>
		<th>IMAP <span class="badge badge-success">{{_("Recommended")}}</span></th>
		<th>POP3</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<th>
			{{ _("Hostname") }}
		</th>
		<td>
			{{ $auth->hostname("mail", true) }}
		</td>
		<td>
			{{ $auth->hostname("mail", true) }}
		</td>
	</tr>

	<tr>
		<th>
			{{ _("Login") }}
		</th>
		<td>
			{{ $auth->username() }}{{ '@' . $auth->domain() }}
		</td>
		<td>
			{{ $auth->username() }}{{ '@' . $auth->domain() }}
		</td>
	</tr>


	<tr>
		<th>
			{{ _("Alternative login") }}
		</th>
		<td>
			{{ $auth->username() }}#{{ $auth->domain() }}
		</td>
		<td>
			{{ $auth->username() }}#{{ $auth->domain() }}
		</td>
	</tr>

	<tr>
		<th>
			{{ _("Password") }}
		</th>
		<td>
			<em>
				{{ _("Password used to access panel") }}
			</em>
		</td>
		<td>
			<em>
				{{ _("Password used to access panel") }}
			</em>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<h5 class="mb-0">Advanced</h5>
		</td>
	</tr>
	<tr>
		<th>
			{{ _("Port") }} ({{ _("secure") }})
		</th>
		<td>
			@if ($auth->hostname("mail", true) !== $auth->serverName())
				993
			@else
				143
			@endif
		</td>
		<td>
			@if ($auth->hostname("mail", true) !== $auth->serverName())
				995
			@else
				110
			@endif
		</td>
	</tr>

	<tr>
		<th>
			{{ _("Connection security") }}
		</th>
		@if ($auth->hostname("mail", true) !== $auth->serverName())
			<td>
				✅ SSL ("SSL/TLS")
			</td>
			<td>
				✅ SSL ("SSL/TLS")
			</td>
		@else
			<td>
				✅ SSL ("STARTTLS")
			</td>
			<td>
				✅ SSL ("STARTTLS")
			</td>
		@endif
	</tr>

	<tr>
		<th>
			{{ _("Authentication type") }}
		</th>
		<td>
			Password ("plain-text")
		</td>
		<td>
			Password ("plain-text")
		</td>
	</tr>

	<tr>
		<th>
			S/MIME
		</th>
		<td>
			❌ {{ _("Disabled") }}
		</td>
		<td>
			❌ {{ _("Disabled") }}
		</td>
	</tr>
	</tbody>
</table>

<div class="d-flex align-items-center align-content-around mb-2">
	<h4 class="mb-0 mr-2">
		{{ _("Outgoing mail") }}
	</h4>
</div>
<table class="table table-striped table-responsive">
	<tbody>
	<tr>
		<th>
			{{ _("Hostname") }}
		</th>
		<td>
			{{ $auth->hostname('smtp', true) }}
		</td>
	</tr>

	<tr>
		<th>
			{{ _("Login") }}
		</th>
		<td>
			{{ $auth->username() }}{{ '@' . $auth->domain() }}
		</td>
	</tr>


	<tr>
		<th>
			{{ _("Alternative login") }}
		</th>
		<td>
			{{ $auth->username() }}#{{ $auth->domain() }}
		</td>
	</tr>

	<tr>
		<th>
			{{ _("Password") }}
		</th>
		<td>
			<em>
				{{ _("Password used to access panel") }}
			</em>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<h5 class="mb-0">Advanced</h5>
		</td>
	</tr>
	<tr>
		<th>
			{{ _("Port") }} ({{ _("secure") }})
		</th>
		<td>
			@if ($auth->hostname("smtp", true) !== $auth->serverName())
				465
			@else
				587
			@endif
		</td>
	</tr>

	<tr>
		<th>
			{{ _("Connection security") }}
		</th>
		@if ($auth->hostname("smtp", true) !== $auth->serverName())
			<td>
				✅ SSL ("SSL/TLS")
			</td>
		@else
			<td>
				✅ SSL ("STARTTLS")
			</td>
		@endif
	</tr>

	<tr>
		<th>
			{{ _("Authentication type") }}
		</th>
		<td>
			Password ("plain-text")
		</td>
	</tr>
	</tbody>
</table>

<div class="d-flex align-items-center align-content-around mb-2">
	<h4 class="mb-0 mr-2">
		{{ _("Security") }}
	</h4>
</div>
<table class="table table-responsive table-striped">
	@includeWhen(MAIL_DEFAULT_SPF, 'partials.mail.spf')
	@includeWhen(MAIL_DEFAULT_DMARC, 'partials.mail.dmarc')
	@includeWhen($auth->hasDkim(), 'partials.mail.dkim')
</table>
