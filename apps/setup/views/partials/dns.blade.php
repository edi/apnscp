@component('partials.about', ['service' => 'DNS']) @endcomponent

<table class="table table-responsive table-striped">
	<tr>
		<th>
			{{_("Nameservers")}}
		</th>
		<td>
			{{ implode(', ', $auth->getNameservers($domain)) }}
		</td>
	</tr>
	@if (!$auth->usesNameservers($domain))
		<tr>
			<th>
				{{_("Detected nameservers")}}
			</th>
			<td>
				{{ implode(', ', $auth->getAssignedNameservers($domain)) }}
			</td>
		</tr>
	@endif
	@if ($ipv4 = $auth->dns_get_public_ip())
	<tr>
		<th>
			IPv4 address
		</th>
		<td>
			{{ $ipv4 }}
		</td>
	</tr>
	@endif
	@if ($ipv6 = $auth->dns_get_public_ip6())
		<tr>
			<th>
				IPv6 address
			</th>
			<td>
				{{ $ipv6 }}
			</td>
		</tr>
	@endif
</table>