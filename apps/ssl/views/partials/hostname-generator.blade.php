<h4>Hostname Validity</h4>
<ul class="certificate-hosts list-unstyled list-inline"
    id="sslSubjects">@foreach ($certificate->getHostnames() as $hostname)
		<li class="pb-2 list-inline-item">
			<button type="button"
			        class="ui-action ui-action-label btn btn-secondary warn ui-action-delete">
				<input type="hidden" name="hostname[]" value="{{ $hostname }}"/>
				{{ $hostname }}
			</button>
		</li>@endforeach</ul>
<h6 class="text-danger empty-notice">
	<i class="fa fa-exclamation-triangle"></i>
	No hostnames configured. Add some below.
</h6>
