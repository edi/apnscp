@foreach ($Page->getProcesses() as $proc)
	<div class="row process align-items-center" data-pid="{{ $proc['pid'] }}">
		<div class="col-2" data-toggle="tooltip" title="PID: {{ $proc['pid'] }}">
			<i class="fa fa-{{ \Frontend\Css\Iconifier::getProcessIconFromName($proc['comm']) }}"></i>
			{{ $proc['comm'] }}
		</div>
		<div class="col-2 text-right">{{ sprintf("%d", ceil($proc['rss']/1024)) }} MB</div>
		<div class="col-2 text-right">{{ $proc['utime'] }} sec</div>
		<div class="col-2 hidden-sm-down">{!! $Page->formatTime($proc['startutime']) !!}</div>
		<div class="col-2 user-col hidden-sm-down">{{ $Page->getUserFromUid($proc['user']) }}</div>
		<div class="col-2 center">
			<button class="btn btn-secondary kill btn-sm"
		        data-toggle="kill" value="{{ $proc['pid'] }}" name="kill">
				<i class="ui-action ui-action-kill ui-action-d-compact"></i> kill
			</button>
		</div>
	</div>
@endforeach