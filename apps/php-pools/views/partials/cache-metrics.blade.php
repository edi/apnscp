<table class="table text-center">
	<thead>
	<th>Memory utilization</th>
	<th>Cache hit rate</th>
	<th>Peak throughput</th>
	<th>Wasted %</th>
	</thead>
	<tbody>
	<td>
		{{
			sprintf(
				"%.2f%%",
				array_get($metrics, 'memory_usage.used_memory', 0)
					/
				array_get($metrics['directives'], 'opcache.memory_consumption', 1) * 100
			)
		}}
	</td>
	<td>
		{{
			sprintf("%.2f%%",
				array_get($metrics, 'opcache_statistics.hits', 0)
					/
				(array_get($metrics,'opcache_statistics.hits', 0) + array_get($metrics, 'opcache_statistics.misses', 0)) * 100
			)
		}}
	</td>
	<td>
		{{
			Formatter::commafy(
				sprintf(
					"%.2f req/sec",
					array_get($metrics, 'opcache_statistics.hits', 0)/($_SERVER['REQUEST_TIME'] - array_get($metrics, 'opcache_statistics.start_time', 0))
				)
			)
		}}
	</td>
	<td>
		{{
			sprintf(
				"%.2f%%",
				array_get($metrics, 'memory_usage.wasted_memory', 0)
					/
				array_get($metrics['directives'], 'opcache.memory_consumption', 1) * 100
			)
		}}
	</td>
	</tbody>
</table>