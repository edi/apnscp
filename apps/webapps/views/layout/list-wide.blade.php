<div class="col-12 col-md-6 col-xl-4 ui-webapp-panel">
	<div class="row">
		@include('master::partials.shared.wa-screenshot', [
			'pane' => $pane,
			'actions' => [
				'view' => 'layout.list-actions',
				'dropdownLocation' => 'dropdown-menu-right',
				'btnGroupClass' => 'btn-group-sm'
			]
		])
	</div>
</div>