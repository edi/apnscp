<?php
?>
<!-- main object data goes here... -->
<script language="javascript" type="text/javascript">
	function delete_list(list_name) {
		document.majordomo.delete_list.value = list_name;
		document.majordomo.submit();
	}
</script>

<form method="POST" name="majordomo">
	<?php
    if (isset($_GET['edit_users'])):
    ?>
	<h3>User Membership</h3>
	<div class="row">
		<fieldset class="col-12 col-md-6">
			<textarea class="col-12" rows="20" name="email_list"><?=$Page->get_users($_GET['edit_users']);?></textarea>
		</fieldset>
		<div class="offset-md-2 col-md-4 hidden-sm-down">
			<p class="note">
				<b>Important reminder:</b> each e-mail account should reside on its own line.
				Each user named in the list to the left may send and receive e-mail to
				<?=$Page->get_list_address()?>.
				<br/><br/>
				You may also edit this file, <a class="ui-action ui-action-switch-app ui-action-label"
				                                href="/apps/filemanager?f=/var/lib/majordomo/lists/<?=$_GET['edit_users']?>">/var/lib/majordomo/lists/<?=$_GET['edit_users']?></a>,
				through the &quot;File Manager&quot;
				this control panel or by downloading the file via <a class="ui-action ui-action-label ui-action-kb"
				                                                     href="<?=MISC_KB_BASE?>/ftp/accessing-ftp-server/">FTP</a>.
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-md-6">
			<button type="submit" name="save_users" class="px-5 mt-3 btn btn-primary" value="Save Changes">
				Save Changes
			</button>
			<button type="reset" name="reset" class="btn btn-secondary mt-3 ml-5 warn" value="Reset">
				Reset
			</button>
			<a href="<?=\HTML_Kit::page_url_params(array('edit_users' => null))?>"
			   class="btn ml-5 mt-3 btn-outline-secondary">Return to Overview</a>
		</div>

	</div>

	<?php
    elseif (isset($_GET['edit_config'])):
    ?>
	<div class="row">
		<div class="col-12 col-md-4">
			<h4>Directive</h4>
		</div>
		<div class="col-12 col-md-8">
			<h4>Info</h4>
		</div>
	</div>
	<?php
            $idx = 1;
            foreach($Page->parse_config($_GET['edit_config']) as $option => $values) {
	printf('
	<div class="row  config-row">' .
		'
		<fieldset for="%s" class="col-12 col-md-4 form-group"><b>%s</b>%s</fieldset>
		' .

		'
		<div class="col-12 col-md-8 help">%s (%s)</div>
	</div>
	',
	$option, $option,$Page->render_html($option,$values), $values['help'],
	$Page->convert_type_to_string($values['type']));
	$idx++;
	}
	?>
	<div class="row">
		<div class="col-12">
			<button type="submit" value="Save Changes" class="btn btn-primary mt-3" name="save_config">
				Save Changes
			</button>
			<button type="reset" class="btn btn-secondary warn ml-5 mt-3" value="Reset">
				Reset
			</button>
			<a href="<?=\HTML_Kit::page_url_params(array('edit_config' => null))?>"
			   class="btn ml-5 mt-3 btn-outline-secondary">Return to Overview</a>
		</div>
	</div>
	<?php
    else:
    ?>
	<h3>Mailing Lists</h3>
	<table class="table">
		<thead>
		<tr>
			<th width="90%">
				List Name
			</th>
			<th class="actions center" width="150">
				Actions
			</th>
		</tr>
		</thead>
		<?php
                foreach ($Page->get_lists() as $list): ?>
		<tr>
			<td>
				<?=$list?>
			</td>
			<td align="center">
				<div class="input-group">
					<div class="btn-group">
						<a title="edit group" class="btn btn-secondary ui-action ui-action-edit-group ui-action-label"
						   href="majordomo.php?edit_users=<?=$list?>">
							Edit Membership
						</a>
						<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
						        aria-haspopup="true" aria-expanded="false">
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<div class="dropdown-menu  dropdown-menu-right">
							<a title="edit options" class="dropdown-item ui-action ui-action-edit ui-action-label"
							   href="majordomo.php?edit_config=<?=$list?>">
								Edit
							</a>
							<a title="delete list" class="dropdown-item ui-action ui-action-delete ui-action-label"
							   href="#"
							   OnClick="confirm('Are you sure you want to delete the mailing list?  There are no undos.') && delete_list('<?=$list?>');">
								Delete
							</a>
						</div>
					</div>
				</div>

			</td>
		</tr>
		<?php endforeach; ?>
	</table>

	<div class="row">
		<div class="col-12 col-md-6">
			<h4>Create Mailing List</h4>
			<fieldset class="form-group">
				<label for="new_list">Mailing List Name</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-tag"></i></span>
					<input class="form-control" name="new_list" type="text" id="new_list" value=""/>
				</div>
			</fieldset>

			<fieldset class="form-group">
				<label for="domain">Mailing List Domain</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-cloud"></i></span>
					<select name="domain" id="domain" class="form-control custom-select">
						<?php foreach($Page->get_domains() as $domain) { printf('
						<option value="%s">%s</option>
						',$domain,$domain); } ?>
					</select>
				</div>
			</fieldset>

			<fieldset class="form-group">
				<label for="admin_email">Administrative E-mail</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input class="form-control" name="admin_email" size="25" value="<?=$Page->get_email()?>"
					       id="admin_email" type="text"/>
				</div>

			</fieldset>

			<fieldset class="form-group">
				<label for="list_password">Administrative Password</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					<input name="list_password" class="form-control" type="password" id="list_password" value=""/>

				</div>

			</fieldset>

		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<button type="submit" class="btn btn-primary px-5 mt-1" value="Create List" class="primary" name="create">
				Create List
			</button>
			<input type="hidden" name="delete_list" value=""/>

		</div>
	</div>
	<?php endif; ?>
</form>
