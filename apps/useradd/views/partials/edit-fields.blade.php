<div class="row">
	<fieldset class="form-group col-12 col-lg-8">
		<label class="">Username</label>
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-user"></i></div>
			@if ($Page->get_mode() == 'add')
				<input class="form-control" name="username" value="{{ $Page->get_option('username') }}" id="username"/>
			@else
				<input type="text" class="form-control col-12" name="username"
				       value="{{ $Page->get_option('username') }}" id="username" disabled="disabled"/>
				<button class="btn btn-secondary change" type="button" id="change-username">
					<i class="ui-action ui-action-edit"></i>
					Change
				</button>
			@endif
		</div>
		@if ($Page->get_mode() != 'add')
			<div id="change-warning" class="collapse">
				<h4 class="mt-1">Important Rules</h4>
				<ol class="rules">
					<li>This process will change the username associated with {{ $Page->get_option('username') }}.
					<li>Any corresponding logins, including e-mail, will be changed to this new username.
					<li>Any e-mail accounts that reference this user will be converted to the new username. If you
						need to keep this e-mail address, visit
						Mail &gt; <a class="ui-action-label ui-action-switch-app ui-action"
						             href="{{ Template_Engine::init()->getPathFromApp('mailboxroutes') }}">Manage
							Mailboxes</a>
						to recreate the old e-mail address.

				</ol>
					<label class="pl-0 form-inline custom-checkbox custom-control align-items-center mr-0 d-flex">
						<input type="checkbox" id="agreeUsername" value="agree"
					       class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						I understand the rules!
					</label>
			</div>
		@endif
	</fieldset>
</div>

<div class="row">
	<fieldset class="form-group col-12 col-lg-8">
		<label class="">Full Name</label>
		<div class="input-group">
			<div class="input-group-addon">
				<i class="fa fa-info"></i>
			</div>
			<input class="form-control" type="text" name="gecos" value="{{ $Page->get_option('gecos') }}"
			       id="gecos"/>
		</div>
	</fieldset>
</div>

<div class="row">
	<fieldset class="form-group col-12  col-lg-8">
		<div class="">
			<label>
				Optional Contact Email
			</label>
			<span class="ml-3" data-toggle="tooltip"
			      title="An email allows the user password reset + security notifications">
	                    <i class="fa fa-sticky-note-o"></i>
	                    What's this?
	                </span>
		</div>
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
			<input class="form-control" type="text" name="email" value="{{ $Page->get_option('email') }}"
			       id="email"/>
		</div>
	</fieldset>
</div>

<div class="row align-items-center">
	<fieldset class="form-group col-12 col-lg-8 has-feedback" id="password-feedback-group">
		<label class="">Password</label>
		<div class="input-group">
			<div class="input-group-addon"><i class="fa fa-lock"></i></div>
			<input type="password" data-minlength="6" class="form-control"
		       name="password" value="{{ $Page->get_option('password') }}" id="password"
				@if ($Page->get_mode() =='add') required @endif />

				@if ($Page->getMode() === 'add')
					<div class="input-group-addon form-inline options border-left-0">
						<label for="randomPassword" class="custom-control custom-checkbox  mr-0 mb-0 align-items-center">
							<input type="checkbox" name="random-password" value="1" id="randomPassword" class="custom-control-input"/>
							<span class="custom-control-indicator"></span>
							<span class="" data-toggle="tooltip" data-title="Password will be displayed in postback summary">
								{{ _("Random password") }}
							</span>
						</label>
					</div>
				@endif

		</div>

		<div class="help-block col-12">
			{{ \ArgumentFormatter::format("%d character minimum",  [\Auth_Module::MIN_PW_LENGTH]) }}
		</div>
	</fieldset>

	<fieldset class="form-group col-12  col-lg-8">
		<div class="password-confirm-container row">
			<label class="col-12">Verify Password</label>
			<div class="col-12 input-group">
				<div class="input-group-addon"><i class="fa fa-lock"></i></div>
				<input type="password" data-match-error="Password does not match"
				       class="form-control form-control-error"
				       data-match="#password" name="password_confirm" value="" id="password_confirm"
						@if ($Page->get_mode() == 'add')  required @endif />
			</div>
			<div class="help-block col-12">&nbsp;</div>
		</div>
	</fieldset>
	<div class="col-12 help-block with-errors text-danger"></div>

</div>