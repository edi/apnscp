
@foreach ($jobs as $cronjob)
	@php
		$timespec = implode(" ",
			array(
				$cronjob['minute'],
				$cronjob['hour'],
				$crontjob['dom'] ?? $cronjob['day_of_month'],
				$cronjob['month'],
				$cronjob['dow'] ?? $cronjob['day_of_week']
			)
		);
		$origjob = array(
			'cmd'  => $cronjob['cmd'],
			'time' => $timespec
		);
		$formats = $Page->formatCron($timespec);
	@endphp
	<form method="post">
		<div class="task row @if ($cronjob['disabled']) text-muted @endif mb-2">
			<div class="col-12 col-md-9 col-sm-8">
				<fieldset class="form-group  mb-0">
					<div class="input-group mask-readonly cmd">
						<div class="input-group-addon">
							<i class="fa fa-terminal"></i>
						</div>
						<input type="text" name="cmd"
						       class="px-2 form-control form-control-lg @if ($cronjob['disabled']) text-muted @endif"
						       readonly value="{{ $cronjob['cmd'] }}"/>
					</div>

					<div class="input-group mask-readonly spec">
						<div class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</div>
						<input type="text" name="time"
						       class="px-2 form-control @if ($cronjob['disabled']) text-muted @endif" readonly
						       value="{{ $timespec }}"/>
					</div>
				</fieldset>
				<fieldset class="form-group mb-0">
					@if ($cronjob['disabled'])
						<label class="form-control-label pl-0">
							TASK DISABLED
						</label>
					@else
						<label class="form-control-label pl-0">
							Last Run
						</label>
						<span class="last  mr-1  @if (null === $formats) text-danger font-weight-bold font-italic @endif">
							{{ $formats['last'] ?? _('BAD TIMESPEC') }}
					    </span>
						<label class="form-control-label pl-0">
							Next Run
						</label>
						<span class="next @if (null === $formats) text-danger font-weight-bold font-italic @endif">
							{{ $formats['next'] ?? _('BAD TIMESPEC') }}
						</span>
					@endif
				</fieldset>
			</div>

			<div class="actions col-12 col-md-3 col-sm-4 text-md-right text-left">
				<input type="hidden" name="Original_State" value="{{ base64_encode(serialize($origjob)) }}"/>
				<div class="input-group">
					<div class="btn-group">
						<button type="button" class="ui-action ui-action-run ui-action-label btn-secondary btn"
						        data-command="{{ $cronjob['cmd'] }}">
							Run Now
						</button>
						<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
						        aria-haspopup="true" aria-expanded="false">
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<button class="dropdown-item ui-action ui-action-run btn-block ui-action-label hidden-md-up"
							        type="button" name="run-active">
								Run Now
							</button>
							<button class="dropdown-item ui-action ui-action-edit btn-block ui-action-label"
							        type="button" name="edit">
								Edit
							</button>
							<button class="dropdown-item ui-action btn-block @if ($cronjob['disabled']) ui-action-enable @else ui-action-disable @endif ui-action-label"
							        name="{{ $cronjob['disabled'] ? 'enable' : 'disable' }}" type="submit">
								@if ($cronjob['disabled'])
									Enable
								@else
									Disable
								@endif
							</button>
							<div class="dropdown-divider"></div>
							<button class="dropdown-item ui-action btn-block ui-action-delete ui-action-label btn-block warn"
							        type="submit" name="delete">
								<i class="fa fa-times"></i>
								Delete
							</button>
						</div>

					</div>
				</div>
			</div>
			<p class="msg col-12"></p>
		</div>
	</form>
@endforeach