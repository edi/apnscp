<div id="theme" class="tab-pane" role="tabpanel" aria-labelledby="">
	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0" name="pref{{ \HTML_Kit::prefify(\Frontend\Tour::ALWAYS_SHOW) }}" />
			<input class="form-check-input custom-control-input" type="checkbox" value="1"
			       name="pref{{ \HTML_Kit::prefify(\Frontend\Tour::ALWAYS_SHOW) }}"
					@if (\Preferences::get(\Frontend\Tour::ALWAYS_SHOW, false)) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip"
			   title="Always show onboarding tours if available for an app."></i>
			Always show onboarding tours
		</label>
	</div>

	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0" name="pref{{ \HTML_Kit::prefify(\Page_Renderer::THEME_SWAP_BUTTONS) }}"/>
			<input class="form-check-input custom-control-input" type="checkbox" value="1"
				name="pref{{ \HTML_Kit::prefify(\Page_Renderer::THEME_SWAP_BUTTONS) }}"
				@if (\Preferences::get(\Page_Renderer::THEME_SWAP_BUTTONS)) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip"
			   title="Switch orientation of menu from left to right."></i>
			Swap menu orientation
		</label>
	</div>

	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0"
		       name="pref{{ \HTML_Kit::prefify(\Page_Renderer::THEME_MENU_ALWAYS_COLLAPSE) }}"/>
			<input class="form-check-input custom-control-input" type="checkbox" value="1"
			       name="pref{{ \HTML_Kit::prefify(\Page_Renderer::THEME_MENU_ALWAYS_COLLAPSE) }}"
			       @if (\Preferences::get(Page_Renderer::THEME_MENU_ALWAYS_COLLAPSE)) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip" title="Always start with menu collapsed."></i>
			Always collapse menu
		</label>
	</div>

	<div class="form-check">
		<label class="custom-control custom-checkbox mb-0 align-items-center">
			<input type="hidden" value="0"
			       name="pref{{ HTML_Kit::prefify(Page_Renderer::THEME_EXTERNAL_OPENER) }}" />
			<input class="form-check-input custom-control-input" type="checkbox" value="1"
			       name="pref{{ HTML_Kit::prefify(Page_Renderer::THEME_EXTERNAL_OPENER) }}"
					@if (Page_Renderer::externalOpener()) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			<i class="fa fa-sticky-note-o mr-1" data-toggle="tooltip" title="Links that go offsite will use an external opener target."></i>
			Use external opener, append
			<small class="ml-1 ui-action ui-action-visit-site-tab ui-action-label"></small>
		</label>
	</div>

	@includeWhen(\Frontend\Css\StyleManager::allowSelection(), 'partials.theme-chooser')

</div>