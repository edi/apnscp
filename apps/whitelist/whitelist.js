$(document).ready(function () {
	apnscp.hinted();
	$('#ui-app .ui-action').ajaxWait();
});

$('document').ready(function() {
	$('.bindable').on('click', function(e) {
		e.preventDefault();
		switch ($(e.target).attr('name')) {
			case 'add':
				return add($('#ipInput').val(), $('#ipInput'));
			case 'remove':
				return remove($(this).val(), $(this));
			default:
				console.log("Unknown bound action " + $(e.target).attr('name'));
				return true;
		}
	});
	$('#active .row :submit').each(function(idx, elem) {
		resolveIp($(elem).data('ip')).done(function (resp) {
			if (!resp.return) {
				return;
			}
			resolveIp($(elem).data('ip'));
			$(elem).siblings('.host').text(resp.return)
		});
	});
})

function add(ip, $self) {
	apnscp.cmd('rampart_whitelist', [ip], null, {useCustomHandlers: true}).then(function (data, textStatus, jqXHR) {
		var found = false;
		$('#active .row :submit').each(function (idx, e) {
			if (e.value === ip) {
				found = true;
				return false;
			}
		});
		if (found) {
			throw "IP " + ip + " is already whitelisted";
		}
		$('#ipInput').val("").effect("transfer", {to: $('#active') }, 500, function() {
			var $elem = $('#item-template').clone(true).data('ip', ip).removeAttr('id').
			find('.ip').text(ip).end().find(':submit').val(ip).end().removeClass('hide');
			$('#active').append($elem);
			updateCounter();
		});
	}).fail(function (jqXHR, textStatus, errorThrown) {
		var reason = [jqXHR];
		if (typeof jqXHR !== 'string') {
			// invalid IP, bad response from API
			reason = $.parseJSON(jqXHR.responseText)['errors'] || [];
		}
		for (var i in reason) {
			$("#errors").append($('<li>').text(reason[i]));
		}
		$('#ipInput').one('keypress', function() {
			$('#errors').empty();
		})

	});
	return false;
}

function remove(ip, $self) {
	apnscp.cmd('rampart_whitelist', [ip, 'remove']).done(function(e) {
		$self.closest('.row').fadeOut('fast', function() {
			$(this).remove();
			updateCounter();
		});

	});
	return false;
}

function updateCounter() {
	$('#counter').text($('#active .row').length + "");
}

function resolveIp(ip) {
	var key = 'rdns-' + ip, hash = localStorage.getItem(key) || {};
	if ("null" !== hash && typeof hash === 'string') {
		var dfd = $.Deferred();
		return dfd.resolve({success: true, "return": hash});
	}
	return apnscp.cmd('dns_gethostbyaddr_t', [ip, 5000], function (resp) {
		if (!resp.success) {
			return;
		}
		var hostname = resp.return;
		try {
			localStorage.setItem(key, hostname);
		} catch(e) {
			if (e === (DOMException.QUOTA_EXCEEDED_ERR || 22)) {
				localStorage.clear();
			}
		}
	});
}