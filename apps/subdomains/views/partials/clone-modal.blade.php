<div class="py-1 hide" tabindex="-1" role="dialog" id="cloneSubdomainModal">
	<form action="{{ HTML_Kit::page_url_params() }}" method="post">
		<h3>Select source/target</h3>
		<div class="d-flex">
			<label class="form-control-label flex-column">
				Source
				<select name="src-domain" class="form-control custom-select mb-3">
					@foreach ($Page->list_domains() as $domain)
						<option value="{{ $domain }}">
							{{ $domain }}
						</option>
					@endforeach
				</select>
			</label>
			<label class="flex-column">
				<span class="form-control-label d-flex p-2 ui-action-transfer ui-action ui-action-d-compact"></span>
			</label>
			<label class="form-control-label flex-column">
				Clone into
				<select name="target-domain" class="form-control custom-select mb-3">
					@foreach ($Page->list_domains() as $domain)
						<option value="{{ $domain }}">
							{{ $domain }}
						</option>
					@endforeach
				</select>
			</label>

		</div>

		<label class="custom-checkbox custom-control align-items-center d-flex">
			<input type="checkbox" name="confirm" id="confirmRecordImport" class="custom-control-input"/>
			<span class="custom-control-indicator"></span>
			<span>
				All subdomains on <span class="dest"></span> will be removed. Subdomains from source
				<span class="source"></span> will be used.
			</span>
		</label>
	</form>
</div>
