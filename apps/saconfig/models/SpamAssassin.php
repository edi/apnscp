<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */

namespace apps\saconfig\models;

class SpamAssassin {
	public const OPENING_MARKER = '# SACONFIGSTART';
	public const CLOSING_MARKER = '# SACONFIGEND';

	protected $defaults = [
		'spam_score'              => 5,
		'tagging_method'          => 'prefix_spam',
		'tagging_method_txt'      => '',
		'mailbox_type'            => 'imap',
		'mail_storage'            => 'maildir',
		'lda_type'                => 'maildrop',
		'mailbox_home'            => 'Mail/',
		'delivery_mailbox'        => 'move_spam',
		'delivery_custom_mailbox' => '',
		'move_custom_mailbox'     => '',
		'delivery_bias'           => true,
		'delivery_bias_delete'    => true,
		'delivery_bias_mailbox'   => '',
		'delivery_bias_score'     => 8.0,
		'bayesian_filtering'      => true,
		'bayes_auto_learn'        => true,
		'bayesian_rules'          => true,
		'bayes_min_ham'           => 1.0,
		'bayes_min_spam'          => 8.0,
		'bayes_spam_activate'     => 200,
		'bayes_ham_activate'      => 200,
		'use_safe'                => 1,
		'use_razor'               => 1,
		'use_pyzor'               => 1,
		"output"                  => "apply",
	];

	protected $settings;
	protected $config;
	protected $recipe;

	public function __construct(array $input = [])
	{
		$this->defaults['delivery_bias_score'] = \Opcenter\Mail\Services\Spamassassin::present() ? 8 : 25;
		$input = array_intersect_key($input, $this->defaults) ?: $this->defaults;

		// synthetic array of negativity, in case a checkbox isn't passed
		$tmp = array_combine(array_keys($this->defaults), array_fill(0, count($this->defaults), false));
		$this->settings = array_merge($tmp, $input);
		$this->process();
	}

	public function process()
	{
		// initialize
		$saconfig = $recipe = self::OPENING_MARKER . "\n" .
				"# Automatically generated on " . date('Y/m/d') . "\n";

		$saconfig .= "# Make sure we get the required score header." . "\n" .
			"add_header all Score _SCORE_" . "\n";

		// Step One:
		// check the spam score input
		$spam_score = $this->settings['spam_score'];
		$saconfig .= "required_score " . (int)$spam_score . "\n";

		// check the subject tagging behavior
		$rewriteStr = "[SPAM] (_SCORE_)";
		$tagging_method = (string)$this->settings['tagging_method'];
		$tagging_method_txt = (string)$this->settings['tagging_method_txt'];
		if ($tagging_method == "prefix_custom") {
			$rewriteStr = $tagging_method_txt;
		} else if ($tagging_method == "prefix_spam") {
			$rewriteStr = "[SPAM] (_SCORE_)";
		} else if ($tagging_method == "prefix_traditional") {
			$rewriteStr = "[SPAM]";
		}

		$saconfig .= "rewrite_header subject " . $rewriteStr . "\n";

		// Step Two:
		// now IMAP check
		$mailbox_type = $this->settings['mailbox_type'];

		// Delivery actions...
		$delivery_mailbox = (string)$this->settings['delivery_mailbox'];
		$delivery_bias = (int)$this->settings['delivery_bias'];
		$delivery_bias_delete = (int)$this->settings['delivery_bias_delete'];
		$delivery_bias_mailbox = (string)$this->settings['delivery_bias_mailbox'];
		$delivery_bias_score = (float)$this->settings['delivery_bias_score'];
		$move_custom_mailbox = (string)$this->settings['move_custom_mailbox'];
		$lda_type = $this->settings['lda_type'];
		$mail_storage = $this->settings['mail_storage'];
		$maildir = ($mail_storage != 'mbox');
		$mailbox_home = $this->settings['mailbox_home'];
		$procmail = ($lda_type == 'procmail');

		if ($mailbox_type != "pop3" || $delivery_mailbox == "delete") {
			// moves to custom mailbox if score exceeds threshold
			if ($delivery_bias) {
				if ($delivery_bias_delete) {
					$delivery_bias_mailbox = '_DELETE_';
				} else  if (!preg_match('/^[a-z\-\[\]_+0-9.]+$/i', $delivery_bias_mailbox)) {
					return error("Invalid score-dependent mailbox name.");
				}
				$recipe .= \a23r::get_class_from_module('spamfilter')::THRESHOLD_VAR .
					"=" . $delivery_bias_score . "\n";
			}

			$recipe .= "if (/^X-Spam-Flag: YES/)" . "\n" . "{" . "\n";

			if ($delivery_bias) {
				$recipe .= "\t/X-Spam-Score: ([-\\.\\d]+)/" . "\n" .
					"\tif (\$MATCH1 >= \$" . \a23r::get_class_from_module('spamfilter')::THRESHOLD_VAR . ")" . "\n" .
					"\t{" . "\n" .
					"\t\t" . ((strtoupper($delivery_bias_mailbox) == "_DELETE_") ?
						"to /dev/null" :
						"to \$HOME/" . $delivery_bias_mailbox) ."\n";
			}

			if ($delivery_mailbox != "nothing") {
				if ($delivery_bias) {
					$recipe .= "\n\t}\n\telse" . "\n" .
						"\t{" . "\n";
				}

				switch ($delivery_mailbox) {
					// user opts to delete e-mails marked as spam
					case "delete":
						$recipe .= ($delivery_bias ? "\t\t" : "\t") . 'to /dev/null'. "\n";
						break;
					// user opts to move e-mail to mailbox named "Spam"
					case "move_spam":
						$recipe .= ($delivery_bias ? "\t\t" : "\t") . 'to $HOME/'. $mailbox_home . '.Spam' . "\n";
						break;
					// user opts to move e-mail to a custom mailbox
					case "move_custom":
						if (!preg_match('/^[a-z@\-\[\]_+0-9.]+$/i', $move_custom_mailbox)) {
							error("Invalid custom mailbox name.");
							$move_custom_mailbox = "";
						}
						$recipe .= ($delivery_bias ? "\t\t" : '') . 'to $HOME/' . $mailbox_home . '.' .
							ltrim($move_custom_mailbox, '.') . "\n";
						break;
				}
			}
			if ($delivery_bias) {
				$recipe .= "\t}\n";
			}

			$recipe .= "}" . "\n";
		}


		// Step Three:
		// Bayesian filtering...
		$bayesian_filtering = (int)($this->settings['bayesian_filtering']);
		$bayes_auto_learn = (int)($this->settings['bayes_auto_learn']);
		$bayesian_rules = (int)($this->settings['bayesian_rules']);
		$bayes_spam_activate = $this->settings['bayes_spam_activate'];
		$bayes_ham_activate = $this->settings['bayes_ham_activate'];
		$bayes_min_spam = $this->settings['bayes_min_spam'];
		$bayes_min_ham = $this->settings['bayes_min_ham'];
		$use_safe = (int)($this->settings['use_safe']);
		$use_razor = (int)($this->settings['use_razor']);
		$use_pyzor = (int)($this->settings['use_pyzor']);
		if ($use_safe) {
			if ($this->settings['use_safe'] == "broken") {
				$use_safe = 2;
			}
		}

		if ($bayesian_filtering) {
			$saconfig .= "use_bayes 1" . "\n";
			if ($bayesian_rules) {
				$saconfig .= "use_bayes_rules 1" . "\n";
			} else {
				$saconfig .= "use_bayes_rules 0" . "\n";
			}

			if ($bayes_auto_learn) {
				$saconfig .= "bayes_auto_learn 1" . "\n";
				if ($bayes_min_ham >= $bayes_min_spam) {
					error("Non-sensical value for threshold of spam/ham.  Ham score should be less than spam score.");
				} else {
					$saconfig .= "bayes_auto_learn_threshold_nonspam " . $bayes_min_ham . "\n" .
						"bayes_auto_learn_threshold_spam " . $bayes_min_spam . "\n";
				}
			} else {
				$saconfig .= "bayes_auto_learn 0" . "\n";
			}

			if (!is_numeric($bayes_ham_activate) || !is_numeric($bayes_spam_activate) ||
				strlen((string)$bayes_spam_activate) == 0 || strlen((string)$bayes_ham_activate) == 0)
			{
				error("Minimum number of spams and hams must be a number.");
			} else {
				$saconfig .= "bayes_min_ham_num " . $bayes_ham_activate . "\n" .
					"bayes_min_spam_num " . $bayes_spam_activate . "\n";
			}
		} else {
			$saconfig .= "use_bayes 0" . "\n" .
				"bayes_auto_learn 0" . "\n" .
				"use_bayes_rules 0" . "\n";
		}

		// safe reporting
		$saconfig .= "report_safe " . $use_safe . "\n";

		// razor
		$saconfig .= "use_razor2 " . $use_razor . "\n";
		$saconfig .= "use_pyzor " . $use_pyzor . "\n";

		$recipe .= self::CLOSING_MARKER . "\n";
		$saconfig .= self::CLOSING_MARKER . "\n";
		$this->recipe = $recipe;
		$this->config = $saconfig;

		return true;
	}

	public function getSettings()
	{
		return $this->settings;
	}


	public function getSetting($name)
	{
		return $this->settings[$name] ?? null;
	}

	/**
	 * Get generated .spamassassin/user_prefs
	 *
	 * @return array
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * Get generated recipe
	 *
	 * @return mixed
	 */
	public function getRecipe()
	{
		return $this->recipe;
	}

	public function getLDAFilter()
	{
		return $this->settings['lda_type'] === "maildrop" ?
			'.mailfilter' : 'procmailrc';
	}
}