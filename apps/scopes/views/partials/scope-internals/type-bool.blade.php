<label class="custom-switch custom-control mb-0 pl-0 mr-3">
	<input type="hidden" name="{{ $name }}" value="0"/>
	<input {{ $attrs ?? '' }} type="checkbox" @if ($value) checked @endif
	class="custom-control-input self-submit" name="{{ $name }}" value="1"/>
	<span class="custom-control-indicator align-self-start"></span>
	Enable
</label>