@foreach ($value as $k => $v)
	<div class="input-group mr-3 mb-3">
		@if (!is_int($k))
			<span class="input-group-addon">PHP {{ $k }}</span>
		@endif
		<select name="args[{{ $k }}]" class="custom-select" @if ($v === "system") DISABLED @endif>
			@if ($v === "system")
				<option value="system">system</option>
			@endif
			@foreach(['native', 'package', 'disabled'] as $type)
				<option value="{{ $type === 'disabled' ? false : $type }}"
			        @if ($v === $type) SELECTED @endif
				>{{ $type }}</option>
			@endforeach
		</select>
	</div>
@endforeach