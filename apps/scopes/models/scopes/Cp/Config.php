<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
	 */

	namespace apps\scopes\models\scopes\Cp;

	use apps\scopes\models\Scope;

	class Config extends Scope
	{
		private const HELP_CACHE = 'apps.scopes';
		/**
		 * @var array
		 */
		protected $help = [];
		protected $readonly = [];

		public function __construct(string $name)
		{
			parent::__construct($name);
			ksort($this->value);
			$this->help = $this->loadComments();
		}

		private function loadComments(): array {
			$lines = file(config_path('config.ini'), FILE_SKIP_EMPTY_LINES|FILE_IGNORE_NEW_LINES);
			$cache = \Cache_Global::spawn();
			if (false !== ($help = $cache->get(self::HELP_CACHE))) {
				$this->readonly = $help['readonly'];
				return $help['built'];
			}

			$built = [];
			$buffer = [];
			$readonly = $section = null;
			foreach ($lines as $line) {
				if (!isset($line[1])) {
					continue;
				}
				if ($line[0] === '[') {
					$section = substr($line, 1, strrpos($line, ']') - 1);
					continue;
				}
				if ($line[0] === ';') {
					if ($line[1] !== ' ') {
						// dump buffer
						$buffer = [];
						$readonly = false;
						continue;
					}
					if ($line === '; PROTECTED') {
						$readonly = true;
						continue;
					}
					$buffer[] = substr($line, 2);
				} else if (ctype_alnum($line[0]) && false !== ($pos = strpos($line, '='))) {
					$key = trim(substr($line, 0, $pos));
					array_set($built, "$section.$key", rtrim(implode(" ", $buffer)));
					if ($readonly) {
						$this->readonly["$section.$key"] = 1;
					}
					$buffer = [];
					$readonly = false;
				}
			}
			$cache->set(self::HELP_CACHE, ['built' => $built, 'readonly' => $this->readonly], 900);
			return $built;

		}

		public function getVariableHelp(string $section, string $var): string
		{
			return $this->help[$section][$var] ?? '';
		}

		public function isReadOnly(string $section, string $var) {
			return isset($this->readonly["${section}.${var}"]);
		}

		public function set($val)
		{
			$config = new \Opcenter\Admin\Settings\Cp\Config();
			$config->setRestart(false);
			if (isset($val[1]) && is_string($val[1])) {
				$val[1] = trim($val[1]);
			}
			return $config->set(...$val);
		}
	}