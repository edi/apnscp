<div class="card">
	<div class="card-header" role="tab" id="headingNetwork">
		<h5 class="mb-0">
			<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#network"
			   aria-expanded="true" aria-controls="network">
				Network
			</a>
		</h5>
	</div>

	<div id="network" class="collapse" role="tabpanel" aria-labelledby="headingNetwork">
		<div class="card-block">
			<table width="100%">
				<thead>
				<tr>
					<th class="">
						Device
					</th>
					<th class="">
						Received (packets)
					</th>
					<th class="">
						Sent (packets)
					</th>
					<th class="">
						Err/Drop
					</th>
				</tr>
				</thead>
				<?php
				$netDevs = $Page->network_information();
				foreach ($netDevs as $dev => $info):
				?>
				<tr>
					<td align="center">
						<?=$dev?>
					</td>
					<td>
						<?=\Formatter::reduceBytes($info['rx_bytes'], 3)?>
						(<?=\Formatter::commafy($info['rx_packets'])?>)

					</td>
					<td>
						<?=\Formatter::reduceBytes($info['tx_bytes'], 3)?>
						(<?=\Formatter::commafy($info['tx_packets'])?>)
					</td>
					<td>
						<?=\Formatter::commafy($info['rx_errs'] + $info['tx_errs'])?>
						/<?=\Formatter::commafy($info['rx_drop'] + $info['tx_drop'])?>
					</td>

				</tr>
				<?php
				endforeach;
				?>
			</table>
		</div>
	</div>
</div>