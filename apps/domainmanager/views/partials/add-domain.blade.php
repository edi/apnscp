<form method="post" data-toggle="validator" id="addForm" action="{{ $DEFAULT_FORM_ACTION }}">
	<h3>Add Domain</h3>
	<div class="row clearfix form-group">
		<div class="col-12 col-lg-6 col-xl-4">
			<label>
				Domain Name
			</label>
			<div class="input-group">
				<label for="domain" class="hinted">example.com</label>
				<span class="input-group-addon"><i class="fa fa-cloud"></i></span>
				<input class="form-control" type="text" name="domain" id="domain"/>
			</div>
		</div>

		<div class="hidden-lg-up mt-3 col-12"></div>

		<div class="col-12 col-lg-6 col-xl-4">
			<div class="row" id="documentRootCustomize" role="tablist" aria-multiselectable="true">
				<div aria-expanded="false" data-parent="#documentRootCustomize" id="user-home-extra"
				     class="collapse type-extra col-12">
					<label class="" data-toggle="tooltip"
					       title="All content for the addon domain will be served from this user's directory">
						<i class="fa fa-sticky-note-o"></i> Document Root
					</label>
					<div class="input-group form-inline">
						<div class="input-group">

							<span class="input-group-addon"><i class="fa fa-user"></i></span>
							<select class="form-control custom-select" style="" id="user-list" name="user_home">
								@foreach($users as $user => $pwdb)
									<option value="{{ $pwdb['home'] }}">{{ $user }}</option>
								@endforeach
							</select>
						</div>
						<div class="input-group">
							<span class="input-group-addon hidden-xs-down ui-action-label hidden-lg-down" id="userPath">
                                <i class="fa fa-folder"></i>
                                /home/foo
                            </span>
							<input type="text" name="user_dir" id="user_path" value="" class="form-control"/>
							<span class="btn-group">
								<button id="browse-home" type="button" class="btn btn-secondary" name="browse_path"><i
											class="fa fa-folder-open"></i>Browse</button>
							</span>
						</div>


					</div>
				</div>

				<div aria-expanded="false" data-parent="#documentRootCustomize" role="tabpanel" id="subdomain-extra"
				     class="collapse type-extra col-12">
					<label>Subdomain</label>
					<select name="subdomain_path" class="form-control custom-select">
						@foreach($Page->get_subdomains() as $subdomain => $location): ?>
							<option value="{{ $location }}">{{ $subdomain }}</option>
						@endforeach
					</select>
				</div>

				<div aria-expanded="true" data-parent="#documentRootCustomize" role="tabpanel"
				     class="type-extra collapse col-12 show" id="site-root-extra">
					<label class="" data-toggle="tooltip"
					       title="All content for the addon domain will be served from here">
						<i class="fa fa-sticky-note-o"></i>
						Document Root
					</label>

					<span class="input-group" role="group">
						<span class="input-group-addon  hidden-xs-down ui-action-label flex-row">
                            <i class="fa fa-folder align-self-center mr-1"></i>
                            /var/www/
                        </span>
				        <input type="text" class="form-control" name="doc_root_path" id="doc_root_path" value=""/>
                        <button id="browse-docroot" type="button" class="btn btn-secondary" name="browse_path">
                            <i class="fa fa-folder-open"></i>
                            Browse
                        </button>
				    </span>

				</div>
			</div>
		</div>

		<div class="hidden-lg-up mt-3 col-12"></div>

		<div class="col-12 col-md-6 col-xl-3 form-group">
			<label class="col-12 hidden-sm-down">&nbsp;</label>
			<div class="btn-group">
				<button type="submit" tabindex="1" name="add" id="submitBtn" class="btn primary" value="Add Domain">
					<i class="hidden-md-down ui-action-label ui-action-add "></i>
					Add Domain
				</button>
				<button class="btn btn-secondary dropdown-toggle ui-action ui-action-advanced ui-action-label"
				        data-toggle="collapse" aria-controls="advancedOptions" data-target="#advancedOptions"
				        aria-expanded="true">
					<span class="sr-only">Toggle Advanced</span>
				</button>
			</div>
		</div>
	</div>

	<div class="row form-group">
		@if ($users || $subdomains)
		<div class="col-12 col-md-6 form-group">
			<label>Addon Domain Location</label>
			<select data-toggle="collapse" class="form-control custom-select" name="shared_type" id="type">
				<option value="doc_root" data-target="#site-root-extra" id="site-root">Under /var/www</option>
				@if (count($users))
					<option value="user_dir" data-target="#user-home-extra" id="user-home">User Home</option>
				@endif
				@if (count($subdomains))
					<option value="subdomain" data-target="#subdomain-extra" id="subdomain">Attach to Subdomain</option>
				@endif
			</select>
		</div>
		@else
			<input type="hidden" name="shared_type" value="doc_root"/>
		@endif
	</div>

	@include('partials.add.advanced')
</form>